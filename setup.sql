-- MySQL
-- sudo mysql -uroot
create database if not exists temperaturverlauf;
create user 'chris'@'localhost' identified by 'geheim';
grant usage on temperaturverlauf.* to 'chris'@'localhost';
grant all privileges on temperaturverlauf.* to 'chris'@'localhost';
flush privileges;
