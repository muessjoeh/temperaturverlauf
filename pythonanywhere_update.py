#!/usr/bin/env python
"""Update the data in the database in pythonanywhere"""
import datetime
import os
import requests

import lib.conf


class APIAction:
    """Class to access the Pythonanywhere API"""
    def __init__(self, username: str, token: str):
        self.username = username
        self.token = token

    def __repr__(self):
        return f"{self.username} {self.token}"

    def get_domain_name(self) -> str:
        return f"{self.username}.pythonanywhere.com"

    def action(self, method: str, action: str) -> str:
        if method.upper() == "GET":
            procedure = requests.get
        elif method.upper() == "POST":
            procedure = requests.post
        else:
            raise RuntimeError(f"Unknown method {method}")

        # See https://help.pythonanywhere.com/pages/API
        # The username is usually the domain_name
        url = f'https://www.pythonanywhere.com/api/v0/user/{self.username}/webapps/{self.get_domain_name()}/{action}/'
        headers = {'Authorization': f'Token {self.token}'}
        # print(f"  {url=}, {headers=}, {procedure=}")
        response = procedure(url, headers=headers)
        if response.status_code == 200:
            return response.text
        else:
            raise RuntimeError(f'Got unexpected status code {response.status_code}: {response.text}')


print("Preparing...")

# Fetch the necessary stuff out of the configuration file
configuration = lib.conf.Configuration()
configuration.use_default_configuration_file()

username = 'temperaturverlauf'
token = configuration.get("token")
assert token
# print(f"{token=}")

# Assert the correct directory
assert os.getcwd() == "/home/temperaturverlauf/devel/temperaturverlauf"

# Assert that the virtual environment is loaded
assert os.environ.get("VIRTUAL_ENV")

# Make the API object ready
api = APIAction(username, token)
# print(f"{api=}")

print("Saving old status...")
# Hole und speichere https://temperaturverlauf.pythonanywhere.com/status
response = requests.get("https://temperaturverlauf.pythonanywhere.com/status")
date_string = datetime.date.today().strftime("%Y%m%d")
with open(f"status/status.{date_string}", "wb") as status_file:
    status_file.write(response.content)

print("Disabling the app...")
# Disable (API: POST disable)
api.action("post", "disable")

print("Cleaning up the file system...")
# Various actions on the file system
# rm -rf data.old
os.system("rm -rf data.old")
# mv data data.old
os.system("mv data data.old")
# rm -rf cache
os.system("rm -rf cache")

print("Fetching the data...")
# fetch.py
os.system("./fetch.py")
print("Persisting the data...")
# persist.py
os.system("./persist.py")

print("Re-enable the web app again...")
# Enable (API: POST enable)
api.action("post", "enable")

print("Access the page and test that it looks okay...")
# Test https://temperaturverlauf.pythonanywhere.com
response = requests.get("https://temperaturverlauf.pythonanywhere.com/")
assert 50_000 < len(response.text) < 100_000
assert "DWD" in response.text

