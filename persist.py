#!/usr/bin/env python
import bz2
import csv
import datetime
import logging
import os.path
import re
import time
from pathlib import Path
from sqlalchemy import and_
from tqdm import tqdm

import lib.dbmodels
import lib.dbutils
from lib.connection import SessionServer
from lib.utils import file_transparent_opener, read_from_zip, Counter, contains_any_substring, \
    inventory_of_weather_phenomenon_types, zip_directory, CachedStationDict

import lib.conf
import lib.logger

configuration = lib.conf.Configuration()
log = logging.getLogger(__name__)
configuration.use_default_configuration_file()

QUALITY_TYPES = {"QN_3", "QN_7", "QN_8", "QN_9"}

persisted_counties = {}
persisted_station_types = {}
persisted_county_types = {}
persisted_stations = {}


def station_type_is_known(name):
    return persisted_station_types.get(name, None) is not None


def county_type_is_known(name):
    return persisted_county_types.get(name, None) is not None


def read_zip_file(file_name, datafile_name, metafile_name, station_id):
    """Extract the data from the ZIP file, and store the type information if necessary"""
    log.debug(f"Reading input file {file_name}")
    persist_metadata_from_csv(file_name, metafile_name)
    persist_measurements_from_csv(str(read_from_zip(file_name, datafile_name), 'latin1'), station_id, file_name.name)


def is_measurement_in_db(session, station_id, measurementtype_id, date):
    """Is the measurement of a specific type for a station and a specific date already in the DB?"""
    return session.query(lib.dbmodels.StationMeasurement.station_id). \
               filter(and_( \
        (lib.dbmodels.StationMeasurement.station_id == station_id),
        and_( \
            (lib.dbmodels.StationMeasurement.measurementtype_id == measurementtype_id),
            lib.dbmodels.StationMeasurement.date == date \
            ))). \
               first() is not None


def persist_metadata_from_csv(file_name, metafile_name):
    meta_content = str(read_from_zip(file_name, metafile_name), 'latin1')
    csv_reader = csv.reader(meta_content.splitlines(), delimiter=';')
    for type in csv_reader:
        if len(type) <= 4:  # The CSV has not enough entries. So, it belongs probably to the footer
            continue
        if type[4] == "Parameter":  # This line belongs to the header
            continue
        if not station_type_is_known(type[4]):  # This line contains a type description that is not yet persisted
            with SessionServer() as session:
                measurement_type = lib.dbmodels.MeasurementTypeOfStation \
                    (name=type[4], description=type[5], unit=type[6])
                log.debug(f"persisting type {measurement_type}")
                session.add(measurement_type)
                session.commit()
                persisted_station_types[measurement_type.name] = measurement_type.id


def persist_measurements_from_csv(string, station_id, file_name):
    log.debug(f"Persisting a string of length {len(string)} of measurements for station {station_id}")

    with SessionServer() as session:
        att_name_line = (string.splitlines())[0]
        log.debug(f"Attribute line: {att_name_line}")
        attributes = att_name_line.split(';')
        # Make sure that some attribute names are as expected
        assert attributes[0].strip() == 'STATIONS_ID'
        assert attributes[1].strip() == 'MESS_DATUM'
        assert attributes[2].strip() in QUALITY_TYPES, \
            f"Unknown quality '{attributes[2]}'. The attribute line was '{attributes}'"
        # All attribute names except a few at the beginning and the end
        types = [attribute.strip() for attribute in attributes[3:] if attribute != 'eor']
        log.debug(f"Attribute names: {types}")

        sm_bulk = []
        commit_countdown = Counter(500)  # Every this perform a commit
        for line in tqdm(string.splitlines()[1:], desc=file_name):
            entries = line.split(';')
            assert int(entries[0]) == station_id
            date = datetime.datetime.strptime(entries[1], "%Y%m%d%H")
            quality = int(entries[2])
            for index, measurement in enumerate(entries[3:]):
                if commit_countdown.round():
                    log.debug("Commit in between")
                    session.bulk_save_objects(sm_bulk)
                    sm_bulk.clear()
                    session.commit()
                if measurement.strip() == 'eor':
                    continue
                # log.debug(f"station_id: {station_id}, Typ; {types[index]},
                # Typ ID: {persisted_types[types[index]]}")
                sm = lib.dbmodels.StationMeasurement(station_id=station_id,
                                                     measurementtype_id=persisted_station_types[types[index]],
                                                     date=date,
                                                     quality=quality,
                                                     value=float(measurement.strip()))
                if sm.value != -999:
                    # Do not store invalid measurements
                    if "_akt" in file_name:
                        if not is_measurement_in_db(session, sm.station_id, sm.measurementtype_id, sm.date):
                            # Do not store already existing entries.
                            # No need to check this with historical files
                            sm_bulk.append(sm)
                    else:
                        sm_bulk.append(sm)  # In historical files, store the measurement unconditionally
        log.debug("Final commit")
        session.bulk_save_objects(sm_bulk)
        sm_bulk.clear()
        session.commit()  # Store the values in the DB


def persist_stations():
    log.info("Processing all station description files")
    for description_filename in Path('data/stations').rglob("*Stationen*"):
        if "#" in description_filename.name:
            continue  # When the file is an editor intermediate file, ignore it
        log.info(f"Working with file {description_filename}")
        with SessionServer() as session:
            opener = file_transparent_opener(description_filename.name)

            with opener(description_filename.absolute(), 'rt', encoding='iso-8859-1') as file_handle:
                if "PH_" in str(description_filename):
                    persist_stations_phenomena(description_filename, file_handle, session)
                else:
                    persist_stations_weather(description_filename, file_handle, session)
            session.commit()


def persist_stations_weather(display_name, file_handle, session):
    log.debug(f"Persisting {display_name} as text")
    do_persist = False
    for line in tqdm(file_handle, desc=display_name.name):
        if "----" in line:  # Start to persist only after the header lines
            do_persist = True
            continue
        if do_persist:
            station = lib.dbmodels.Station()
            station.build_from_text(line)
            persist_station(session, station)


def persist_stations_phenomena(display_name, file_handle, session):
    log.debug(f"Persisting {display_name} as dict")

    reader = csv.DictReader(file_handle, delimiter=";", skipinitialspace=True)
    rows = [{k.strip(): v.strip() for k, v in row.items()} for row in reader]
    for line in tqdm(rows, desc=display_name.name):
        station = lib.dbmodels.Station()
        log.debug(f"Building a station from the dict: {line}")
        station.build_from_dict(line)
        persist_station(session, station)


def persist_station(session, station):
    station_from_db = session.query(lib.dbmodels.Station).get(station.id)
    if station_from_db is None:
        session.add(station)  # Station is not yet in the DB, so store it
        log.debug(f"Persisting {station}")
        station_from_db = session.query(lib.dbmodels.Station).get(station.id)
    persisted_stations[station_from_db.id] = station_from_db


def persist_county_measurements():
    log.info("Processing all county data files")
    with SessionServer() as session:
        # Persist the county weather phenomena types
        for type in inventory_of_weather_phenomenon_types.values():
            db_type = lib.dbmodels.MeasurementTypeOfCounty(name=type[0], unit=type[1])
            session.add(db_type)
            session.commit()
            persisted_county_types[type[0]] = db_type.id

    for filename in sorted(Path('data/county_averages').rglob("regional*")):
        # Collect the file names in the county averages directory
        if "#" in filename.name:
            continue  # When the file is an editor intermediate file, ignore it
        log.debug(f"Working with file {filename}")
        # This has to be hard coded, because I know no place where this information is located
        if "_rr_" in filename.name:
            measurement_type_description = "Niederschlag"
        elif "_sd_" in filename.name:
            measurement_type_description = "Sonnenscheindauer"
        elif "_tm_" in filename.name:
            measurement_type_description = "Lufttemperatur"
        elif contains_any_substring(filename.name, inventory_of_weather_phenomenon_types.keys()):
            # Other types, like 'Frosttage' or 'Hitzetage'
            method_name = filename.name.partition("regional_averages_")[2].partition("_year.txt")[0]
            measurement_type_description = inventory_of_weather_phenomenon_types[method_name][0]
            assert measurement_type_description is not None
        else:
            raise NameError(f"Unknown type of entries in {filename.name}")

        log.debug(f"Persisted types: {persisted_county_types}; persisted counties: {persisted_counties}")
        with SessionServer() as session:
            opener = file_transparent_opener(filename.name)
            with opener(filename.absolute(), 'rt', encoding='iso-8859-1') as file_content:
                do_persist = False  # Do not persist the header lines
                cm_bulk = []
                for line in tqdm(file_content, desc=filename.name):
                    if line.startswith("Zeitreihen"):
                        # No interesting stuff found in the 1st line
                        continue
                    if "Bayern" in line:  # Example of a county, so this is the line of counties
                        county_names = line.split(';')[2:-1]  # Without header and footer
                        county_names.append("Hamburg")  # Hard coded
                        county_names.append("Bremen")  # Hard coded
                        county_names.append("Berlin")  # Hard coded
                        for county_name in county_names:
                            county = lib.dbmodels.County(name=county_name)
                            if county_name not in persisted_counties:
                                session.add(county)
                                session.commit()
                                persisted_counties[county.name] = county.id
                                log.debug(f"Caching {county.name}, which has the ID {county.id}")
                    if '0' <= line[0] <= '9':  # Start to persist only after the header lines
                        do_persist = True
                    if do_persist:
                        measurements = line.split(';')
                        year = -1  # dummy value
                        month = -1
                        for pos, value in enumerate(measurements):
                            if pos == 0:
                                year = value
                            if pos == 1 and value != 'year':
                                month = value
                            if pos == len(measurements) - 1:  # The last entry is empty
                                continue
                            if pos >= 2:
                                # The order of the values is the county order that is already known
                                assert year != -1
                                county = list(persisted_counties.keys())[pos - 2]
                                log.debug(f"  Persisting measurement of county {county},"
                                          f" which has the ID {persisted_counties[county]}")
                                measurement = \
                                    lib.dbmodels.CountyMeasurement(county_id=persisted_counties[county],
                                                                   measurementtype_id=
                                                                   persisted_county_types[measurement_type_description],
                                                                   value=value,
                                                                   year=year,
                                                                   month=month)
                                cm_bulk.append(measurement)
            session.bulk_save_objects(cm_bulk)
            cm_bulk.clear()
            session.commit()


def persist_measurements_of_stations():
    log.debug("Entering persist_measurements_of_stations()")
    for description_filename in sorted(Path('data/measurements_of_station').rglob("*.zip")):
        log.debug(f"Processing file {description_filename}")
        if "_TU_" in description_filename.name:
            log.debug(f"Processing temperature file {description_filename.name}")
            file_designator = "tu"
        elif "_RR_" in description_filename.name:
            log.debug(f"Processing precipitation file {description_filename.name}")
            file_designator = "rr"
        elif "_FF_" in description_filename.name:
            log.debug(f"Processing wind file {description_filename.name}")
            file_designator = "ff"
        elif "_SD_" in description_filename.name:
            log.debug(f"Processing sunshine duration file {description_filename.name}")
            file_designator = "sd"
        else:
            log.warning(f"Unknown file {description_filename.name}")
            continue

        internal_file = list(filter(lambda x: "produkt" in x, zip_directory(description_filename.absolute())))[0]
        (station_id) = re.findall("_(\\d+)_", description_filename.name)[0]

        read_zip_file(file_name=description_filename.absolute(),
                      datafile_name=internal_file,
                      metafile_name=f"Metadaten_Parameter_{file_designator}_stunde_{station_id}.txt",
                      station_id=int(station_id))

def persist_phenomena_of_counties():
    """Persist the nature phenomena for every county"""
    # todo Bundesland 'Deutschland'
    with SessionServer() as session:
        station_county = CachedStationDict(session)
        county_germany_id = persisted_counties["Deutschland"]
        assert county_germany_id is not None
        log.debug(f"Deutschland ist {county_germany_id} mit dem Typ {type(county_germany_id)}")
        # Persist the phenomenon types, which are county measurement types

        phenomena_from_configuration = configuration.get('phenomena')
        log.debug(f"{phenomena_from_configuration=}")
        for phenomenon_configuration in phenomena_from_configuration:
            # A configured Phenomenon has e.g. the form "Hasel; 5; Blühbeginn"
            phenomenon_species, phenomenon_phase_id, phenomenon_description =\
                list(split.strip() for split in phenomenon_configuration.split(";"))
            phenomenon_short_name = f"{phenomenon_species} ({phenomenon_description})"
            log.debug(f"Found {phenomenon_short_name} in configuration")
            phenomenon_type =\
                lib.dbmodels.MeasurementTypeOfCounty(name=phenomenon_short_name,
                                                     unit="Tag des Jahres")
            session.add(phenomenon_type)

            for phenomenon_filename in Path('data/station_phenomena').rglob("*.bz2"):
                log.debug(f"Looking at the file {str(phenomenon_filename)}")
                if str(phenomenon_filename).find('_' + phenomenon_species + '_') < 0:
                    # This specific configuration
                    # does not fit to this species specific phenomenon file,
                    # because this is a different species
                    log.debug(f"The file name {phenomenon_filename} "
                              f"does not match the species {phenomenon_species}")
                    continue

                log.debug(f"The file name {phenomenon_filename} "
                          f"matches the species {phenomenon_species}")
                with bz2.open(phenomenon_filename, "rt", encoding='iso-8859-1') as phenomenon_file:
                    county_aggregators = {}
                    sm_bulk = []
                    reader = csv.DictReader(phenomenon_file, delimiter=";", skipinitialspace=True)
                    # Remove all leading and trailing spaces
                    rows = [{k.strip(): v.strip() for k, v in row.items()} for row in reader]
                    log.debug(f"Iterating through the content of file {str(phenomenon_filename)}."
                              f" There are {len(rows)} entries in this file")
                    for line in tqdm(rows, desc=os.path.basename(str(phenomenon_filename))):
                        if line["Phase_id"] != str(phenomenon_phase_id):
                            continue  # This line does not contain the desired phenomenon
                        stations_id = line["Stations_id"]
                        try:
                            county_id = station_county.get(stations_id)
                        except ValueError:
                            county_id = None
                        if county_id is None:
                            log.info(f"The station {stations_id} has no county")
                            continue
                        county_year = line["Referenzjahr"]
                        county_key = f"{county_id}_{county_year}"
                        germany_key = f"{county_germany_id}_{county_year}"
                        log.debug(f"{county_key=}")
                        log.debug(f"{germany_key=}")
                        if not county_key in county_aggregators:
                            # If we had this county not yet, create a new aggregator for it
                            county_aggregators[county_key] = lib.utils.AvgAggregator()
                        if not germany_key in county_aggregators:
                            # If we had germany not yet, create a new aggregator for it
                            county_aggregators[germany_key] = lib.utils.AvgAggregator()
                        county_aggregators[county_key].collect(int(line["Jultag"]))
                        county_aggregators[germany_key].collect(int(line["Jultag"]))
                    log.debug(f"{county_aggregators.keys()=}")
                    for county_key, aggregator in county_aggregators.items():
                        county_id, year = county_key.split('_')
                        measurement = \
                            lib.dbmodels.CountyMeasurement(county_id=county_id,
                                                           measurementtype_id=phenomenon_type.id,
                                                           year=year,
                                                           month=-1,
                                                           value=aggregator.aggregate())
                        sm_bulk.append(measurement)

                session.bulk_save_objects(sm_bulk)
                session.commit()

def empty_database():
    log.debug("Emptying the database")
    with SessionServer() as session:
        start_time = time.time()
        log.debug("  Deleting county measurements")
        print("  Deleting county measurements")
        session.query(lib.dbmodels.CountyMeasurement).delete()
        log.debug("  Deleting counties")
        print("  Deleting counties")
        session.query(lib.dbmodels.County).delete()
        log.debug("  Deleting county measurement types")
        print("  Deleting county measurement types")
        session.query(lib.dbmodels.MeasurementTypeOfCounty).delete()
        log.debug("  Deleting station measurements")
        print("  Deleting station measurements")
        session.query(lib.dbmodels.StationMeasurement).delete()
        log.debug("  Deleting station measurement types")
        print("  Deleting station measurement types")
        session.query(lib.dbmodels.MeasurementTypeOfStation).delete()
        log.debug("  Deleting stations")
        print("  Deleting stations")
        session.query(lib.dbmodels.Station).delete()
        log.debug("  Committing")
        print("  Committing")
        session.commit()
        end_time = time.time()
        log.debug(f" Clearing took {str(datetime.timedelta(seconds=end_time - start_time))}")


def check():
    """Check if everything is ready"""
    data_dir = Path("data")
    if not data_dir.exists():
        raise RuntimeError("The 'data' directory does not exist. Have you executed 'fetch.py'?")
    if len(list(data_dir.glob("*"))) == 0:
        raise RuntimeError("The 'data' directory is empty. Have you executed 'fetch.py'?")


def main():
    start_time = time.time()
    print("Check if everything is okay")
    check()
    print("Clearing database")
    empty_database()
    print("Persisting county measurements")
    persist_county_measurements()
    print("Persisting stations")
    persist_stations()
    print("Persisting phenomena for counties")
    persist_phenomena_of_counties()
    print("Persisting station measurements")
    persist_measurements_of_stations()

    end_time = time.time()
    print(f"It all took {str(datetime.timedelta(seconds=end_time - start_time))}")


if __name__ == '__main__':
    main()
