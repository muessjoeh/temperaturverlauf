#!/usr/bin/env python

import re

# -*- coding: utf-8 -*-

# Usage: python3 generate_image_creator.py > create_graphs.sh

with open("README.md", 'r') as csv_file:
    content = csv_file.read()

for line in content.splitlines():
    command = re.search("Kommandozeile: `(.*)`", line)
    if command is None:
        continue
    print(command.group(1))
