python visualize_bundesland.py --image_name=img/bavaria.mp3
python visualize_bundesland.py --image_name=img/bavaria.png
python visualize_bundesland.py --what=Niederschlag --image_name=img/bavaria_precipitation.png
python visualize_bundesland.py --what=Sonnenscheindauer --image_name=img/bavaria_sunshine.png
python visualize_bundesland.py --what "Hasel (Blühbeginn)" --image_name img/hasel_bluehbeginn_bayern.png
python visualize_bundesland.py --what "Stiel-Eiche (Blattverfärbung)" --image_name img/stieleiche_blattfall_bayern.png
python visualize_station.py  --base=summer --image_name=img/augsburg.png
python visualize_station.py  --group=hour --base=summer --image_name=img/hottesthour.png
python visualize_station.py  --group=hour --base=winter --image_name=img/coldesthour.png
python visualize_station.py  --group=month --image_name=img/hottestmonth.png
python visualize_station.py  --measurement=R1 --image_name=img/augsburg_precipitation.png
python3 visualize_station.py  --base=summer --station=Attenkam --image_name=img/attenkam.png
python3 visualize_station.py  --base=summer --station=Mainz-Lerchenberg\ \(ZDF\) --image_name=img/mainz-lerchenberg.png
python3 visualize_station.py  --base=summer --station=Mittenwald-Buckelwiesen --image_name=img/mittenwald-buckelwiesen.png
python3 visualize_station.py  --base=summer --station=München-Stadt --image_name=img/muenchen-stadt.png
