# Temperaturverlauf

[TOC]

![Temperaturwerte für Bayern](img/bavaria.png)

Grafische Darstellung der Daten des [DWD](http://www.dwd.de) (Deutscher Wetterdienst; sehr seriös), bezogen auf vorzugsweise Augsburg und Bayern (andere sind auswählbar), also direkt vor der Haustüre. Es sind also nicht nur weit entfernte Eis- und Koalabären betroffen, sondern eben auch wir persönlich und unsere Liebsten. Man sieht auch deutlich, dass wir bereits mitten in der Klimakatastrophe stecken und es nicht erst die ferne Zukunft betrifft.

Hier im Projekt werden die Daten des DWD, die als Zahlen vorliegen, grafisch dargestellt. Es sind also generell keine selbst erhobenen Daten oder Modellierungen, sondern es handelt sich um eine Visualisierung von tatsächlichen Messdaten des DWD, die genau so aufgetreten sind.

Dieses Projekt habe ich verfasst, um die abstrakten Aussagen, die man immer hört ("Es wird wärmer/trockener"), mitsamt irgendwelcher Anekdoten ("Hier ist es besonders heiß", "Und hier", "Und hier auch", ...), für mich konkret erfassbar zu machen: Es sind ja immer nur Aussagen aus zweiter Hand, und ich wollte die konkreten Werte selbst vor Augen haben.
Jeder hat ja bestimmt schon mal eine Unterhaltung geführt, bei der das Gegenüber meinte "Nö, an den Klimawandel glaub ich nicht, weil vorgestern in [Hintertupfing](https://www.dwd.de/DE/wetter/wetterundklima_vorort/bayern/hintertupfing/_node.html) war ja Mistwetter!" (Diese Wettersituation kann sich übrigens nicht lediglich über ein paar Tage erstrecken, sondern auch über einen längeren Zeitraum. Es kommt vielmehr auf den [Durchschnitt auf der gesamten Erde](https://climatereanalyzer.org/wx/todays-weather/maps/gfs_world-wt_t2anom_d1.png) an, und nicht nur darauf, was sich direkt vor der Haustür abspielt). Dass diese Argumentation unredlich ist, ist ja klar (es werden ["Klima" und "Wetter" verwechselt](https://www.br.de/nachrichten/wissen/wetter-ist-nicht-gleich-klima-unterschied-kurz-erklaert,TTxMJWW), also etwas ziemlich Grundlegendes), aber wie sieht die Entwicklung des Wettergeschehens hier konkret aus? Um dies selbst besser feststellen und beurteilen zu können, habe ich die in numerischer Form vorhandenen Messdaten des DWD visualisiert (*Ein Bild sagt mehr als 1000 Worte*).
Außerdem wollte ich Fragen wie "Ist es wärmer als vor 1 Jahr?" oder "Und vor 5 Jahren?" beantworten.

Generell gilt: [Listen to science!](https://www.scientists4future.org/)

Die entstehenden Plots können in den Formaten PNG, EPS, PDF, PS, PGF, RAW, RGBA, SVG, SVGZ, WAV und MP3 abgespeichert werden (bei WAV und MP3 sind es Tondateien; je größer der Messwert, desto höher der Ton; [hier das Beispiel für die Lufttemperatur in Bayern](img/bavaria.mp3), das man auch grafisch gleich am Beginn des Textes sieht).

(Kommandozeile: `python visualize_bundesland.py --image_name=img/bavaria.mp3`)

Ich bin zwar Programmierer, aber es ist eines meiner ersten Projekte in der Programmiersprache Python. Ich bin also in der Thematik drinnen, jedoch bin ich ein Neuling lediglich in dieser Programmiersprache. So ähnlich wie ein Jogger, der andere Schuhe verwendet. Es können deshalb also durchaus Formulierungen im Code zu finden sein, derentwegen ich in ein paar Jahren die Hände über dem Kopf zusammenschlagen werde. Aber nichts Grundsätzliches.


#### Interaktive Version im Browser

Eine öffentlich zugängliche [interaktive Version](https://temperaturverlauf.pythonanywhere.com), bei der man verschiedene Messwerte darstellen kann und das ganze direkt hier im Browser sieht, gibt es ebenfalls. Die Fähigkeiten des Programms sind zwar nicht komplett von der Version, die man auf dem heimischen Rechner direkt laufen lassen kann, übertragen worden. Dafür steht sie jedem offen, nicht nur Leuten, die Computernerds sind: Wenn man sich das hier lesen kann, geht die verlinkte interaktive Version ebenfalls.

Diese Web-Version hat nicht alle Fähigkeiten des Programms, sondern lediglich die, die die Ebene der Bundesländer betreffen. Für alles, was mit einzelnen Stationen zu tun hat, muss man das Programm lokal bei sich laufen lassen.


#### Kommentare und Änderungswünsche

Am einfachsten schreibt ihr eine Mail an `temperaturverlauf (ätt) mailbox.org`.

Falls ihr euch mit Gitlab auskennt, könnt ihr natürlich auch ein Issue verfassen.

Ihr könnt irgendwelche Ideen, die ihr gerne noch anhand der Daten grafisch dargestellt haben wollt, vorschlagen. Und ihr könnt auch Fehler, die ihr entdeckt oder sonstige Anmerkungen und Kommentare melden.

Und wer programmieren kann, ist herzlich eingeladen etwas zu ändern oder hinzuzufügen. Man kann auch Code von hier in eigene Projekte übernehmen. Das Projekt steht unter der [MIT-Lizenz](https://opensource.org/licenses/MIT), deren Kurzfassung ist: Mach doch damit, was Du willst.

#### Links

Außerdem kann man die Seite der [Parents for Future Augsburg](https://parentsforfuture.de/de/augsburg) oder der [Fridays for Future Augsburg](https://www.fff-augsburg.de/) besuchen. Und Leuten, denen diese Seite hier gefällt, finden vermutlich auch Gefallen an [Our World in Data](https://ourworldindata.org/), der [Worldometers Seite](https://www.worldometers.info/) und den [Daten des Umweltbundesamtes](https://www.umweltbundesamt.de/daten/umweltindikatoren). Vermutlich findet ihr auch [diesen Vortrag des CCC über die Nutzung von Klimadaten](https://media.ccc.de/v/36c3-10571-nutzung_offentlicher_klimadaten) interessant. Und es gibt noch einen Podcast, der den 6. IPCC Sachstandsbericht [zusammengefasst hat](https://dasklima.podigee.io/) und sich jetzt der allgemeinen wissenschaftlichen Forschung in Sachen Klimakatastrophe widmet. Und man kann auch nachsehen, [wie ambitioniert Klimamaßnahmen](https://climateactiontracker.org/) von einzelnen Ländern verfolgt werden. Man kann sich außerdem auch die passende [Wikipedia Seite über den Zeitverlauf der Lufttemperatur in Deutschland](https://de.wikipedia.org/wiki/Zeitreihe_der_Lufttemperatur_in_Deutschland) und den [Folgen der Klimankatastrophe hierzulande](https://de.wikipedia.org/wiki/Folgen_der_globalen_Erw%C3%A4rmung_in_Deutschland) ansehen. Außerdem dürfte das Projekt [Bits & Bäume](https://bits-und-baeume.org/) auf Interesse stoßen.

#### Verwendete Software

Dieses Projekt verwendet: [`Python`](https://www.python.org), [`Matplotlib`](https://matplotlib.org/), [`SQLAlchemy`](https://www.sqlalchemy.org/), [`SciPy`](https://www.scipy.org/), [`Flask`](https://flask.palletsprojects.com/) und [`MySQL`](https://www.mysql.com) (zur Benutzung anderer Datenbanksysteme muss eine Konfigurationsdatei geändert werden; getestet habe ich auch [`PostgreSQL`](https://www.postgresql.org/)).


#### Benutzung

    git clone git@gitlab.com:muessjoeh/temperaturverlauf.git
    cp configuration_sample.yaml configuration.yaml
    # Erzeugung eines zufälligen Strings.
    # Beim folgenden Schritt kam man den gut gebrauchen
    # Den entstehenden String einfach in die Zwischenablage kopieren
    dd if=/dev/random bs=40 count=1 2> /dev/null | base64
    $EDITOR configuration.yaml
    virtualenv venv
    . venv/bin/activate
    pip install -r requirements.txt
    ./fetch.py
    ./persist.py
    ./visualize_bundesland.py -h
    ./visualize_station.py -h

Wenn man die Darstellung als Webseite aktivieren möchte:

    FLASK_APP=web.bundesland FLASK_ENV=development flask run

Die Seite ist dann unter http://localhost:5000 verfügbar.

## Screenshots

### Bayern und andere Bundesländer

#### Temperatur

Plot der Durchschnitts-Temperaturen pro Jahr von 1881 bis 2020 in Bayern, geglättet durch einen [Savitzky-Golay-Filter](https://de.wikipedia.org/wiki/Savitzky-Golay-Filter). Das Ergebnis ist nicht gerade [überraschend](https://de.wikipedia.org/wiki/Klimastreifen), ist aber von Daten, die von hier aus Bayern stammen und uns daher direkt betreffen. Es handelt sich hierbei insgesamt um eine sehr, sehr beunruhigende Grafik.

Übrigens ist Bayern nicht das einzige Bundesland, das dargestellt werden kann: Das geht mit allen. Bayern wird hier lediglich exemplarisch auf dieser Seite dargestellt. Der Graph der anderen Bundesländer sieht nicht wesentlich anders aus: Wenn ihr das Programm direkt laufen lassen könnt, könnt ihr mal "`--all_counties`" und "`--county`" ausprobieren. Man kann auch mehrmals `--county` angeben, um verschiedene Bundesländer zu vergleichen. Eines dieser Bundesländer heißt übrigens "Deutschland" und ist der Durchschnitt aller Bundesländer. 

Seit 1980 ist die durchschnittliche Temperatur ziemlich kontinuierlich um gut 1,2°C gestiegen (also 0,03°C pro Jahr). Zum Vergleich: Während der letzten Kalt- bzw. Eiszeit sank die durchschnittliche Temperatur des Planeten um [4,5°C](https://de.wikipedia.org/wiki/Letzteiszeitliches_Maximum). Ungefähr ein knappes Drittel Eiszeit haben wir also schon. Und wir stehen kurz vor verschiedenen unumkehrbaren [Kipppunkten](https://www.pik-potsdam.de/de/produkte/infothek/kippelemente/kippelemente), die mit einer plötzlichen Änderung diverser Werte drohen, beispielsweise der Temperatur. Es droht also nicht nur, dass es ein bisschen wärmer wird, sondern plötzlich sehr viel heißer: Ein Anstieg von 3 °C klingt zwar possierlich ("13 °C statt 10 °C im Frühling ist doch toll!"), ist es aber überhaupt nicht. Es sieht viel eher nach einer extrem einschneidenden Zäsur aus, die unser gesamtes Leben (für Wirtschaftsfanatiker: Ja, auch die Wirtschaft) massiv beeinflussen wird.

Man kann sich das eher so vorstellen wie Fieber: Ein Anstieg der Temperatur um lediglich 2° fühlt sich komplett anders an.

Der Klimawandel zeigt sich übrigens nicht unbedingt in einer wärmeren Lufttemperatur: Durch Art der Erderwärmung ist der Temperaturunterschied zwischen Äquator und den Polen geringer (die Pole erwärmen sich schneller), wodurch Wetterlagen länger andauern (es ist also länger heiß oder eben regnerisch; Stichworte [Jetstream](https://de.wikipedia.org/wiki/Jetstream), [Polarwirbel](https://de.wikipedia.org/wiki/Polarwirbel)). Die Ausprägung dieser Wetterlagen ist vom Zufall abhängig. Es kann auch längere Zeit kühl sein, dadurch erscheint die Erderhitzung kurzzeitig und trügerisch weniger ausgeprägt und bedrohlich.

Durch den Treibhauseffekt wirkt die Strahlung der Sonne intensiver. In anderen Worten, der Energie-Eintrag der Sonne ist größer. Dadurch werden Wetterlagen verstärkt: Es herrscht nicht nur Hitze, sondern Bullenhitze, nicht nur Regen, sondern Starkregen mit Überschwemmungen und nicht nur Sturm, sondern ein Tornado. 

Es handelt sich hierbei auch um Durchschnittstemperaturen, die man nicht direkt wahrnehmen kann. Extremwerte im Gegensatz hierzu schon. Und diese sind angestiegen.

Weiterhin ist es so, dass zwar die gesamte Durchschnittstemperatur der Erde angestiegen ist. [Auf den Meeren erhöht sich die durchschnittliche Temperatur aber weniger stark, dafür auf der Landmasse umso stärker](https://de.wikipedia.org/wiki/Seeklima), und [in den Städten kann die Temperatur sogar noch zusätzlich 10°C mehr als auf dem Land betragen (Wärmeinseln)](https://de.wikipedia.org/wiki/Stadtklima). Deshalb kann man auch nicht beruhigt die Werte betrachten, weil sie ja eher klein sind: Wenn die Temperaturen über dem Meer weniger zum Durchschnitt beitragen, dann tun es die Temperaturen über dem Land umso mehr. Kurz: Es wird an Land (also sämtlichen bewohnten Gebieten) noch heißer als es der Durchschnitt suggeriert.

Wenn man Kinder hat, kann man mal ihnen ja mal tief in die Augen blicken und sich dann diese Grafik vor das innere Auge rufen und dabei auf seine Gefühle achten.

Man sieht also ganz deutlich, dass die Klimakatastrophe aber nicht erst unsere Kinder und Enkel betreffen wird, sondern ziemlich akut ist und wir bereits mitten drin sind. Die jüngere Generation aber stärker, weil sie diesen klimatischen Verhältnissen noch vermutlich ein paar Jahre länger ausgesetzt sein wird.

Manche denken, dass bis jetzt kaum gravierende Änderungen bei uns aufgetreten sind. Die Corona-Krise hat uns jedoch demonstriert, dass die stille Annahme falsch ist, die hier gemacht wird: Unsere Welt morgen ist fast genauso wie heute. "Bis jetzt ist es doch gut gegangen!" könnte auch jemandem auf Höhe des 10. Stockwerks durch den Kopf gehen, der gerade aus dem 30. Stockwerk gefallen ist. 

Wenn man die Wetterdaten gedanklich fortschreibt, kommt man zu dem beunruhigendem Ergebnis, dass der Sommer in diesem Jahr vermutlich der kühlste und feuchteste für den Rest unseres Lebens ist.

![Temperaturwerte für Bayern](img/bavaria.png)

[(Daten)](http://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/monthly/air_temperature_mean/). (Es ist die Darstellung Daten des Jahresdurchschnitts oder einer bestimmten Jahreszeit oder Monat möglich. Innerhalb eines einzelnen Tages ist leider keine feinere Unterteilung bei Bundesländern möglich)

(Kommandozeile: `python visualize_bundesland.py --image_name=img/bavaria.png`)

#### Niederschlag

Ebenso gibt es beim DWD die Niederschlagsmengen, die in Bayern im Durchschnitt jedes Jahr niedergehen.

Die Niederschlagsmenge ist etwas schwieriger interpretierbar, denn im Prinzip könnte auch die gesamte Menge am 1. Januar abregnen und für Überschwemmungen sorgen, und dafür dann der Rest des Jahres strohtrocken bleiben.

Wenn man sich den Graphen ansieht, ist es kein Wunder, dass die meisten Pflanzen braun sind, denn seit ca dem Jahr 2000 sind die Niederschläge ständig massiv gesunken. Und man hat kein gutes Gefühl, wenn man sich vorstellt, dass es sich dabei nicht nur um Gras, sondern beispielsweise um Getreide und Kartoffeln handelt, und wenn man den weiteren Trend fortschreibt.

Es sieht auch nicht gerade nach einer kurzen Durststrecke aus, die man halt hinter sich bringen muss, sondern nach einem sehr lang andauernden Trend, der sich so wie es aussieht, noch mindestens etliche Jahre so fortsetzen und verschlimmern wird. Es geht vermutlich weiterhin stetig bergab.

![Niederschläge für Bayern](img/bavaria_precipitation.png)

[(Daten)](http://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/monthly/precipitation/)

(Kommandozeile: `python visualize_bundesland.py --what=Niederschlag --image_name=img/bavaria_precipitation.png`)


#### Sonnenscheindauer

Man kann sich beim DWD auch die durchschnittliche Sonnenscheindauer herunterladen. Die Daten sind hier seit 1951 verfügbar. Man kann deutlich erkennen, dass sie seit 2000 stark zugenommen hat.

Die rechnerisch maximal mögliche Sonnenscheindauer ist übrigens 4380 Stunden pro Jahr. Vermutlich wird dieser Wert in der Sahara tatsächlich erreicht.

![Sonnenscheindauer für Bayern](img/bavaria_sunshine.png)

[(Daten)](http://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/monthly/sunshine_duration/)

(Kommandozeile: `python visualize_bundesland.py --what=Sonnenscheindauer --image_name=img/bavaria_sunshine.png`)


#### Phänomene

Welche Jahreszeit gerade ist, erfährt man normalerweise an den Dingen, die gerade draußen vor sich gehen, und nicht durch einen Blick auf den Kalender. Diese Dinge nennt man Phänomene.

Damit sind so Dinge gemeint wie "Blüten treiben aus" oder "Blätter werden braun". Der DWD hat das schön [zusammengefasst](https://www.dwd.de/DE/klimaumwelt/klimaueberwachung/phaenologie/produkte/phaenouhr/phaenouhr.html?nn=575800). Man sieht hier deutlich, dass sich die kältere Phase des Jahres in den letzten Jahren verkürzt und die wärmere verlängert hat. Dies dürfte auch für Allergiker interessant sein, weil die lästige Zeit pro Jahr länger wird, und auch schon früher beginnt.

Dabei bedeutet `Tag des Jahres` die laufende Nummer des Tags von 1 bis 366. Beispielsweise ist 23 der 23. Januar und 42 der 11. Februar.

Hier werden exemplarisch der Blühbeginn der Hasel als Beginn des Vorfrühlings und die Blattverfärbung der Stiel-Eiche als Anzeiger des Spätherbsts verwendet. Es können natürlich beliebige Messwerte in die Konfiguration aufgenommen werden. Man sieht, dass der Beginn des Frühlings früher und der Herbst später stattfindet, wodurch sich der Herbst und damit auch der Winter verkürzt und der Sommer verlängert. 

![Blühbeginn der Hasel (Vorfrühling)](img/hasel_bluehbeginn_bayern.png)

(Kommandozeile: `python visualize_bundesland.py --what "Hasel (Blühbeginn)" --image_name img/hasel_bluehbeginn_bayern.png`)

[(Historische Daten](https://opendata.dwd.de/climate_environment/CDC/observations_germany/phenology/annual_reporters/wild/historical/PH_Jahresmelder_Wildwachsende_Pflanze_Hasel_1930_2022_hist.txt) und [aktuelle)](https://opendata.dwd.de/climate_environment/CDC/observations_germany/phenology/annual_reporters/wild/recent/PH_Jahresmelder_Wildwachsende_Pflanze_Hasel_akt.txt)

![Blattfall der Stiel-Eiche (Winter)](img/stieleiche_blattfall_bayern.png)

(Kommandozeile: `python visualize_bundesland.py --what "Stiel-Eiche (Blattfall)" --image_name img/stieleiche_blattfall_bayern.png`)

[(Historische Daten](https://opendata.dwd.de/climate_environment/CDC/observations_germany/phenology/annual_reporters/wild/historical/PH_Jahresmelder_Wildwachsende_Pflanze_Stiel-Eiche_1925_2022_hist.txt) und [aktuelle)](https://opendata.dwd.de/climate_environment/CDC/observations_germany/phenology/annual_reporters/wild/recent/PH_Jahresmelder_Wildwachsende_Pflanze_Stiel-Eiche_akt.txt)

### Augsburg und andere Städte

Da ich in Augsburg wohne, wird diese Stadt automatisch genommen, wenn man nichts anderes angibt. Es sind aber auch andere Städte möglich, wenn sich dort oder in der Nähe eine Messstation des DWD befindet. 

Man kann sich mit `visualize_station.py --stations` die gesamten Messstationen des DWD ausgeben lassen, zusammen mit ihrer ID Nummer, unter der man sie beim DWD findet, und der Information, ob die Messwerte hier schon in der Datenbank abgespeichert sind. Falls nicht, muss man lediglich die Konfigurationsdatei erweitern und dann `fetch.py` und danach `persist.py` aufrufen.

Es wird Augsburg genommen, wenn man nichts anderes angibt. Vorher muss man natürlich die entsprechenden Daten in der Datenbank abgespeichert haben, was jeweils ca 50 MB pro Station verbraucht (es gibt vom DWD rund 1100 Messstationen, also wären das für alle ungefähr 55 GB; das ist auch der Grund, wieso nicht alle von diesem Projekt standardmäßig gespeichert werden).

Leider gibt es beim DWD solch granuläre Daten erst ab einem relativ späten Zeitpunkt, für Augsburg beispielsweise ab 1955 (Temperatur) und 1996 (Niederschlag).

#### Temperatur

Plot der Temperaturen der Sommer- oder Wintermonate (Juni, Juli, August bzw Dezember, Januar, Februar) oder des ganzen Jahres in Augsburg, als Box- oder Violin-Plot. In dem Beispiel unten habe ich die Sommertemperaturen mit einem Violin-Plot aufgemalt. Der Strich in der Mitte ist der Median. Ich habe auch eine Regressionsgerade eingezeichnet (in diesen kurzen Zeiträumen kann man vereinfacht einen linearen Zusammenhang annehmen. Die Regressionsgerade wird ab 1980 berechnet, weil ab hier die Werte in den einzelnen Bundesländern stetig ansteigen).

[(Daten; Augsburg ist 00232)](http://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/10_minutes/air_temperature/historical/):

![Temperaturwerte der Sommermonate für Augsburg nach Jahr](img/augsburg.png)

(Kommandozeile: `python visualize_station.py  --base=summer --image_name=img/augsburg.png`)

Die gleichen Daten können auch verwendet werden, um zu schauen, welche Stunden des Tages wie heiß sind: Es gibt offenbar nicht so sehr eine Mittags- als eine Nachmittagshitze.

![Temperaturwerte für Augsburg im Sommer nach Stunde](img/hottesthour.png)

(Kommandozeile: `python visualize_station.py  --group=hour --base=summer --image_name=img/hottesthour.png`)

Und dasselbe auch für den Winter für Leute, die draußen schlafen, wie beispielsweise Klimacamper. 

![Temperaturwerte für Augsburg im Winter nach Stunde](img/coldesthour.png)

(Kommandozeile: `python visualize_station.py  --group=hour --base=winter --image_name=img/coldesthour.png`)


Ebenso kann man schauen, wie hoch die Temperaturen für einen bestimmten Monat sind. Nicht gerade überraschend sind Juni, Juli und August am wärmsten, und Dezember, Januar und Februar am kältesten:

![Temperaturwerte für Augsburg nach Monat](img/hottestmonth.png)

(Kommandozeile: `python visualize_station.py  --group=month --image_name=img/hottestmonth.png`)

Falls die Frage aufkommt, die höchste Temperatur dieses Augsburger Datensatzes ist 37,0°C (27.07.1983), die niedrigste ist -26,6°C (07.01.1985)

#### Niederschlag

Es gibt beim DWD auch die Niederschlagsdaten von Augsburg.

![Niederschlagswerte für Augsburg nach Jahr](img/augsburg_precipitation.png)

(Kommandozeile: `python visualize_station.py  --measurement=R1 --image_name=img/augsburg_precipitation.png`)

[(Daten; Augsburg ist 00232)](http://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/10_minutes/precipitation/historical/).

## Utopie

Man hört öfters, dass eine fossilgetriebene Welt ja unbedingt sein muss, alles andere wäre überhaupt nicht erstrebenswert. Das exakte Gegenteil ist richtig:

Man stelle sich einmal die nähere Zukunft vor, und die Gefahr der Klimakatastrophe ist erfolgreich umschifft worden. 
Man stelle sich nun vor, wie man mit einem Freund an einer ehemals dicht befahrenen Straße flaniert. Und ihr 
erinnert euch gemeinsam daran, dass man sich hier vor ein paar Jahren nicht gerne aufgehalten hat, weil man sich ja eh nicht unterhalten konnte, und es nicht gerade gut gerochen hat. Wenn ihr jetzt schnuppert, riecht es nach frischem Brot, ihr seid nämlich gerade vor einer Bäckerei. Plötzlich brummt es. Du schaust, wo das herkommt: von einem Käfer. Ihr setzt euch auf eine der zahlreichen Sitzbänke, und zwar in den Schatten, weil sie praktischerweise ein Solardach haben, atmet nochmal tief durch und schaut den Leuten zu. Und solche Sitzbänke sind ziemlich zahlreich: Seit das ganze Geld nicht mehr für ausländisches Öl, sondern die heimische Wirtschaft ausgegeben wird, ist ziemlich viel Geld vorhanden.
Außerdem unterhaltet ihr euch über eure Kinder: Durch die beherzten Klimaschutzmaßnahmen werden sie vermutlich glücklicherweise zu ihren Lebzeiten noch erleben können, wie die Wetterkapriolen weniger werden.

Da möchte man doch unbedingt sein!
