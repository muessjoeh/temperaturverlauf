#!/usr/bin/env python


# -*- coding: utf-8 -*-

import argparse
import logging
import time

import lib.dbutils
from lib.plotcounty import PlotCounty
from lib.utils import are_seasons_possible
import lib.logger

DEFAULT_COUNTY = ["Bayern"]


def main():
    cli_timing_begin = time.time()

    parser = create_cli_parser()
    logging.debug(f"Zwischenzeit 0 {time.time() - cli_timing_begin}")
    cli_timing_begin = time.time()
    args = parser.parse_args()

    logging.debug(f"Zwischenzeit 1 {time.time() - cli_timing_begin}")
    cli_timing_begin = time.time()

    known_county_names = lib.dbutils.get_county_names()
    counties = args.county if args.county is not None else DEFAULT_COUNTY
    if counties[0] == "all":
        counties = known_county_names
    county_types = lib.dbutils.get_county_types()
    current_type = None
    for ctype in county_types.keys():
        if args.what == ctype:
            current_type = ctype
            break
    logging.debug(f"Zwischenzeit 2 {time.time() - cli_timing_begin}")
    cli_timing_begin = time.time()
    if current_type is None:
        raise RuntimeError(f"The type '{args.what}' is unknown. Did you run fetch.py and persist.py?")
    if args.base != "year" and not are_seasons_possible(current_type):
        raise RuntimeError(f"The type '{args.what}' is not compatible with seasons.")
    logging.debug(f"Zwischenzeit 3 {time.time() - cli_timing_begin}")
    cli_timing_begin = time.time()
    if args.all_counties:
        print(known_county_names)
        exit(1)
    for c in counties:
        if c not in known_county_names:
            raise RuntimeError(f"The county '{c}' is unknown. I only know {known_county_names}")
    logging.debug(f"Zwischenzeit 4 {time.time() - cli_timing_begin}")
    cli_timing_begin = time.time()
    logging.debug(f"duration command line parsing: {time.time() - cli_timing_begin} ")
    plot_object_creation_begin = time.time()
    plot = PlotCounty(window=args.window, type_indicator=current_type, unit=county_types[current_type],
                      counties=counties, image_name=args.image_name, base=args.base,
                      smoothed=args.just_smoothed, point_density=args.point_density)
    logging.debug(f"duration of plot object creation: {time.time() - plot_object_creation_begin}")
    plot.do_it()


def create_cli_parser():
    parser = argparse.ArgumentParser(description="Graph of various persisted climate data "
                                                 "of the DWD of various counties, default is Bavaria",
                                     epilog='Remember to run "fetch.py" and "persist.py" first!')
    bases = [str(m) for m in range(1, 12 + 1)]
    bases.extend(["year", "spring", "summer", "autumn", "winter"])
    parser.add_argument("--base", dest="base",
                        choices=bases,
                        default="year", help="The measurements that are taken into account. "
                                             "The whole year, the seasons, or one specific month (1-12)")
    parser.add_argument("--what", dest="what",
                        choices=lib.dbutils.get_county_types().keys(),
                        default="Lufttemperatur", help="What measurement to plot")
    parser.add_argument("--window", dest="window",
                        default=30,
                        type=int,
                        help="Width of the smoothing window. The default is 30.")
    parser.add_argument("--county", action="append", dest="county",
                        # default=["bayern"],  # Does not work, because the rest is always appended
                        help="The county (Bundesland) name. 'all' is also valid. "
                             "Providing this parameter more than one time is possible")
    parser.add_argument("--all_counties", action='store_true',
                        help="List all available counties")
    parser.add_argument("--just_smoothed", action='store_true',
                        help="Plot only the smoothed graph")
    parser.add_argument("--image_name", dest="image_name",
                        type=str,
                        help="Output file name of the finished picture of the graph, "
                             "including its type indicator. "
                             "When it is not given, the graph is displayed on screen"
                        )
    parser.add_argument("--point_density", dest="point_density",
                        type=int,
                        default=1,
                        help="Plot only every n-th point"
                        )
    return parser


if __name__ == '__main__':
    main()
