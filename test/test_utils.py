import bz2
import gzip
import logging
import os
import pathlib
import unittest
from collections import namedtuple
from datetime import datetime, date
from unittest import TestCase

from lib import dbmodels
from lib.conf import Configuration
from lib.utils import rolling_mean, split_into_chunks, reduce_list, \
    file_transparent_opener, get_files_in, read_from_zip, \
    season_of_month, completely_in_year, Counter, \
    SumAggregator, AvgAggregator, Aggregator, search_file, \
    translation_en_de, sanitize_string, desanitize_string, contains_any_substring, \
    find_filename_recursively, \
    has_sufficient_entries, seasons, map_range, zip_directory, \
    MaxAggregator, MinAggregator, CachedURL, nearest_odd_number, \
    remove_prefix, parse_date, parse_timerange, reduce_list_density, PersistentCounter


class TestMeasurement:
    def __init__(self, date, value):
        self.date = date
        self.value = value

    def __repr__(self):
        return f"{self.date} {self.value}"

    def __eq__(self, other):
        return self.date == other.date and self.value == other.value


class TestUtils(TestCase):
    def setUp(self):
        self.TestMeasurement2 = namedtuple('TestMeasurement2', 'date value')

    def test_gliding_average(self):
        self.assertEqual([1.5, 2.5, 3.5, 4.5], rolling_mean((1, 2, 3, 4, 5), 2))
        self.assertEqual([2, 3, 4], rolling_mean((1, 2, 3, 4, 5), 3))

    def test_split_into_chunks(self):
        # Usual list
        test_list = [TestMeasurement(datetime(2020, 1, 1), 1),
                     TestMeasurement(datetime(2020, 1, 20), 2),
                     TestMeasurement(datetime(2020, 2, 1), 3),
                     TestMeasurement(datetime(2020, 2, 2), 4)]
        expected_list = [[1, 2], [3, 4]]
        expected_list_sum = [[3], [7]]
        self.assertEqual(expected_list,
                         split_into_chunks(test_list, "value", 30 * 60 * 60 * 24))  # 30 days
        self.assertEqual(expected_list_sum,
                         split_into_chunks(test_list, "value", 30 * 60 * 60 * 24, True
                                           ))

        # The same with named tuples instead of objects
        test_list = [self.TestMeasurement2(date=datetime(2020, 1, 1), value=1),  # Mixed parameters
                     self.TestMeasurement2(datetime(2020, 1, 20), 2),
                     self.TestMeasurement2(datetime(2020, 2, 1), 3),
                     self.TestMeasurement2(datetime(2020, 2, 2), 4)]

        expected_list = [[1, 2], [3, 4]]
        expected_list_sum = [[3], [7]]
        self.assertEqual(expected_list,
                         split_into_chunks(test_list, "value", 30 * 60 * 60 * 24))  # 30 days
        self.assertEqual(expected_list_sum,
                         split_into_chunks(test_list, "value", 30 * 60 * 60 * 24, True
                                           ))

        # The same with '0' as value
        test_list = [TestMeasurement(datetime(2020, 1, 1), 0),
                     TestMeasurement(datetime(2020, 1, 20), 0),
                     TestMeasurement(datetime(2020, 2, 1), 0),
                     TestMeasurement(datetime(2020, 2, 2), 0)]
        expected_list = [[0, 0], [0, 0]]
        expected_list_sum = [[0], [0]]
        self.assertEqual(expected_list,
                         split_into_chunks(test_list, "value", 30 * 60 * 60 * 24))
        self.assertEqual(expected_list_sum,
                         split_into_chunks(test_list, "value", 30 * 60 * 60 * 24, True))

        # The result is only one chunk
        test_list = [TestMeasurement(datetime(2020, 1, 1), 1),
                     TestMeasurement(datetime(2020, 1, 20), 2)]
        expected_list = [[1, 2]]
        expected_list_sum = [[3]]
        self.assertEqual(expected_list,
                         split_into_chunks(test_list, "value", 30 * 60 * 60 * 24))
        self.assertEqual(expected_list_sum,
                         split_into_chunks(test_list, "value", 30 * 60 * 60 * 24, True))

        # Empty list
        self.assertEqual([], split_into_chunks([], "value", 100))  # Just a dummy number
        self.assertEqual([], split_into_chunks([], "value", 100, True))  # Just a dummy number

    def test_reduce_list(self):
        self.assertEqual([1, 2, 3], reduce_list([[1], [2], [3]]))
        # self.assertEqual([[1,2], [3,4]], reduce_list([[[1], [2]], [[3], [4]]]))
        self.assertEqual([], reduce_list([[]]))

    def test_file_transparent_opener(self):
        self.assertTrue(file_transparent_opener("bla.txt.gz") == vars(gzip)["open"])
        self.assertTrue(file_transparent_opener("bla.txt.bz2") == vars(bz2)["open"])
        # I don't know how to test for regular text files

    def test_remote_dir(self):
        # First, no recursion
        files = get_files_in(
            "http://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/precipitation/")
        self.assertGreater(len(files), 0)

        # Then with recursion
        files = get_files_in(
            "http://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/", True)
        self.assertGreater(len(files), 0)
        self.assertIn("http://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/precipitation/regional_averages_rr_year.txt", files)
        self.assertIn("http://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/hot_days/regional_averages_txbs_year.txt", files)
        self.assertNotIn("hot_days", files)  # The directories...
        self.assertNotIn("hot_days/", files)  # should not be there


    def test_read_from_zip(self):
        numbers_file = find_filename_recursively("numbers.zip")
        content = read_from_zip(numbers_file, "one.txt")
        self.assertEqual(str("one"), content.strip().decode())

    def test_zip_directory(self):
        numbers_file = find_filename_recursively("numbers.zip")
        directory = zip_directory(numbers_file)
        self.assertEqual(3, len(directory))
        self.assertIn("one.txt", directory)
        self.assertIn("two.txt", directory)
        self.assertIn("three.txt", directory)

    def test_season_of_month(self):
        self.assertEqual("winter", season_of_month(1))
        self.assertEqual("winter", season_of_month(2))
        self.assertEqual("spring", season_of_month(3))
        self.assertEqual("spring", season_of_month(4))
        self.assertEqual("spring", season_of_month(5))
        self.assertEqual("summer", season_of_month(6))
        self.assertEqual("summer", season_of_month(7))
        self.assertEqual("summer", season_of_month(8))
        self.assertEqual("autumn", season_of_month(9))
        self.assertEqual("autumn", season_of_month(10))
        self.assertEqual("autumn", season_of_month(11))
        self.assertEqual("winter", season_of_month(12))

        with self.assertRaises(AssertionError):
            season_of_month(0)
            season_of_month(13)

    def test_in_year(self):
        self.assertTrue(completely_in_year(1, "year"))
        self.assertFalse(completely_in_year(12, "year"))
        self.assertTrue(completely_in_year(1, "winter"))
        self.assertTrue(completely_in_year(3, "winter"))
        self.assertTrue(completely_in_year(4, "winter"))
        self.assertTrue(completely_in_year(2, "spring"))
        self.assertTrue(completely_in_year(3, "spring"))
        self.assertFalse(completely_in_year(5, "spring"))
        self.assertFalse(completely_in_year(6, "spring"))
        self.assertTrue(completely_in_year(6, "summer"))
        self.assertFalse(completely_in_year(8, "summer"))
        self.assertFalse(completely_in_year(9, "summer"))
        self.assertTrue(completely_in_year(8, "autumn"))
        self.assertTrue(completely_in_year(9, "autumn"))
        self.assertFalse(completely_in_year(11, "autumn"))
        self.assertTrue(completely_in_year(11, "winter"))
        self.assertTrue(completely_in_year(12, "winter"))

        with self.assertRaises(AssertionError):
            completely_in_year(0, "year")
            completely_in_year(0, "spring")
            completely_in_year(0, "summer")
            completely_in_year(0, "autumn")
            completely_in_year(0, "winter")

            completely_in_year(13, "year")
            completely_in_year(13, "spring")
            completely_in_year(13, "summer")
            completely_in_year(13, "autumn")
            completely_in_year(13, "winter")

    def test_search_file(self):
        self.assertIsNotNone(search_file("test/test_utils.py"))
        self.assertIsNone(search_file("xyz"))

    def test_counter(self):
        c = Counter(3)
        self.assertFalse(c.round())
        self.assertFalse(c.round())
        self.assertTrue(c.round())
        self.assertFalse(c.round())
        self.assertFalse(c.round())
        self.assertTrue(c.round())

        c = Counter(100_000)
        for _ in range(99_999):
            self.assertFalse(c.round())
        self.assertTrue(c.round())
        for _ in range(99_999):
            self.assertFalse(c.round())
        self.assertTrue(c.round())

    def test_sanitize_string(self):
        s = "Test"
        s_out = sanitize_string(s)
        self.assertEqual(s, s_out)

        s = "Thueringen"
        s_out = sanitize_string(s)
        self.assertEqual("Thüringen", s_out)

        s = "uebermuedet"
        s_out = sanitize_string(s)
        self.assertEqual("übermüdet", s_out)

        s = "zurueckgedraengt"
        s_out = sanitize_string(s)
        self.assertEqual("zurückgedrängt", s_out)

        s = "schoen"
        s_out = sanitize_string(s)
        self.assertEqual("schön", s_out)

        s = "Niederschlagsdauer"
        s_out = sanitize_string(s)
        self.assertEqual("Niederschlagsdauer", s_out)

    def test_desanitize_string(self):
        s = "Test"
        s_out = desanitize_string(s)
        self.assertEqual(s, s_out)

        s = "Thueringen"
        s_out = desanitize_string(s)
        self.assertEqual(s, s_out)

        s = "Thüringen"
        s_out = desanitize_string(s)
        self.assertEqual('Thueringen', s_out)

        s = "übermüdet"
        s_out = desanitize_string(s)
        self.assertEqual("uebermuedet", s_out)

        s = "zurückgedrängt"
        s_out = desanitize_string(s)
        self.assertEqual("zurueckgedraengt", s_out)

        s = "schön"
        s_out = desanitize_string(s)
        self.assertEqual("schoen", s_out)

    def test_sum_aggregator(self):
        a = SumAggregator()
        a.collect(10)
        a.collect(11)
        a.collect(12)
        self.assertEqual(33, a.aggregate())
        self.assertEqual(3, a.get_length())

    def test_avg_aggregator(self):
        a = AvgAggregator()
        a.collect(10)
        a.collect(11)
        a.collect(12)
        self.assertEqual(11, a.aggregate())
        self.assertEqual(3, a.get_length())

    def test_max_aggregator(self):
        a = MaxAggregator()
        a.collect(10)
        a.collect(11)
        a.collect(12)
        self.assertEqual(12, a.aggregate())
        self.assertEqual(3, a.get_length())

    def test_min_aggregator(self):
        a = MinAggregator()
        a.collect(10)
        a.collect(11)
        a.collect(12)
        self.assertEqual(10, a.aggregate())
        self.assertEqual(3, a.get_length())

    def test_aggregator_factory(self):
        self.assertEqual(SumAggregator, Aggregator.aggregator_factory("stdl. Sonnenscheindauer").__class__)
        self.assertEqual(SumAggregator, Aggregator.aggregator_factory("Niederschlag").__class__)
        self.assertEqual(SumAggregator, Aggregator.aggregator_factory("stdl. Niederschlagshoehe").__class__)
        self.assertEqual(AvgAggregator, Aggregator.aggregator_factory("Lufttemperatur").__class__)
        self.assertEqual(AvgAggregator, Aggregator.aggregator_factory("relative Feuchte").__class__)
        self.assertEqual(AvgAggregator, Aggregator.aggregator_factory("Windgeschwindigkeit").__class__)
        self.assertEqual(AvgAggregator, Aggregator.aggregator_factory("Eistage").__class__)
        self.assertEqual(AvgAggregator, Aggregator.aggregator_factory("Frosttage").__class__)
        self.assertEqual(AvgAggregator, Aggregator.aggregator_factory("Hitzetage").__class__)
        self.assertEqual(SumAggregator, Aggregator.aggregator_factory("Niederschlag >= 10 mm").__class__)
        self.assertEqual(SumAggregator, Aggregator.aggregator_factory("Niederschlag >= 20 mm").__class__)
        self.assertEqual(AvgAggregator, Aggregator.aggregator_factory("Sommertage").__class__)
        self.assertEqual(AvgAggregator, Aggregator.aggregator_factory("Tropennächte").__class__)

    def test_specific_aggregator(self):
        self.assertEqual(SumAggregator, Aggregator.specific_aggregator("sum").__class__)
        self.assertEqual(AvgAggregator, Aggregator.specific_aggregator("avg").__class__)
        self.assertEqual(MaxAggregator, Aggregator.specific_aggregator("max").__class__)
        self.assertEqual(MinAggregator, Aggregator.specific_aggregator("min").__class__)

    def test_human_name(self):
        self.assertEqual("Summe", Aggregator.specific_aggregator("sum").human_name())
        self.assertEqual("Durchschnitt", Aggregator.specific_aggregator("avg").human_name())
        self.assertEqual("Maximum", Aggregator.specific_aggregator("max").human_name())
        self.assertEqual("Minimum", Aggregator.specific_aggregator("min").human_name())

    def test_translation(self):
        self.assertEqual("Jahr", translation_en_de['year'])
        self.assertEqual("Frühling", translation_en_de["spring"])
        self.assertEqual("Sommer", translation_en_de["summer"])
        self.assertEqual("Herbst", translation_en_de["autumn"])
        self.assertEqual("Winter", translation_en_de["winter"])
        self.assertEqual("Januar", translation_en_de["month 1"])
        self.assertEqual("Juli", translation_en_de["month 7"])
        self.assertEqual("Dezember", translation_en_de["month 12"])


    def test_configuration(self):
        print("Testing configuration")
        configuration = Configuration()
        print("Finding configuration.yaml")
        configuration_filename = find_filename_recursively("configuration.yaml", "test")
        print(f"Found configuration file {configuration_filename}")
        configuration.add_file(configuration_filename)
        self.assertFalse(configuration.is_empty())
        self.assertEqual("value1 with spaces", configuration.get("key_str"))
        self.assertEqual(42000, int(configuration.get("key_int")))
        self.assertEqual(3.14, float(configuration.get("key_float")))
        self.assertTrue(bool(configuration.get("key_boolean")))
        self.assertEqual(3, len(configuration.get("key_list")))
        self.assertTrue("value2" in configuration.get("key_list"))
        self.assertEqual((), configuration.get("empty"))
        print("Done testing configuration")

    def test_contains_any_substring(self):
        test_list = ['eins', 'zwei', 'drei']
        self.assertTrue(contains_any_substring("blabla_eins_blubblub", test_list))
        self.assertFalse(contains_any_substring("blabla_acht_blubblub", test_list))

        test_list = ['eins >zwei', 'drei >vier']
        self.assertTrue(contains_any_substring("blabla_eins >zwei_blubblub", test_list))
        self.assertFalse(contains_any_substring("blabla_acht >neun_blubblub", test_list))

    def test_find_filename_recursively(self):
        self.assertIsNotNone(find_filename_recursively("test_utils.py", "test"))
        self.assertIsNone(find_filename_recursively("does_not_exist.txt", "test"))

    def test_has_sufficient_entries(self):
        self.assertTrue(has_sufficient_entries(12, seasons["year"]))
        self.assertTrue(has_sufficient_entries(13, seasons["year"]))
        self.assertFalse(has_sufficient_entries(11, seasons["year"]))
        self.assertTrue(has_sufficient_entries(3, seasons["summer"]))
        self.assertFalse(has_sufficient_entries(2, seasons["summer"]))
        self.assertTrue(has_sufficient_entries(3, seasons["autumn"]))
        self.assertFalse(has_sufficient_entries(2, seasons["autumn"]))
        self.assertFalse(has_sufficient_entries(1, seasons["autumn"]))
        self.assertFalse(has_sufficient_entries(0, seasons["autumn"]))

    def test_map_range(self):
        range1 = (1, 2)
        self.assertEqual(200, map_range(range1, 1))
        self.assertEqual(800, map_range(range1, 2))
        self.assertEqual(500, map_range(range1, 1.5))

    def test_get_content(self):
        # Random HTTP text:
        url = "https://opendata.dwd.de/README.txt"  # This file should be rather constant
        cache_file = "test/fake_cached_file.txt"
        cached = CachedURL(cache_file, url)
        self.assertTrue("fake" in cached.get_content().decode('UTF-8'))

        missing_file = "test/missing_cached_file.txt"
        try:
            not_cached = CachedURL(missing_file, url)
            not_cached_content = not_cached.get_content()
            # Test 2 text snippets to be sure
            self.assertTrue(bytes("shared", "utf8") in not_cached_content, not_cached_content)
            self.assertTrue(bytes("description", "utf8") in not_cached_content, not_cached_content)
            self.assertTrue(os.path.exists(missing_file))  # It should be created be the cache class
            with open(missing_file) as mf:
                # Check the content of the cache
                cache_content = mf.read()
                self.assertTrue("shared" in cache_content, cache_content)
                self.assertTrue("description" in cache_content, cache_content)
        finally:
            os.remove(missing_file)  # It should continue to be missing

    def test_nearest_odd_number(self):
        self.assertEqual(31, nearest_odd_number(30))
        self.assertEqual(31, nearest_odd_number(31))
        with self.assertRaises(AssertionError):
            nearest_odd_number(4.0)

    def test_remove_prefix(self):
        self.assertEqual(remove_prefix("Test ohne Präfix", "Apfel"), "Test ohne Präfix")
        self.assertEqual(remove_prefix("Test mit Präfix", "Test"), " mit Präfix")

    def test_parse_date(self):
        self.assertEqual(parse_date("2023"), date(2023, 1, 1))
        self.assertEqual(parse_date("2023", False), date(2023, 12, 31))
        self.assertEqual(parse_date("02.2023"), date(2023, 2, 1))
        self.assertEqual(parse_date("2.2023"), date(2023, 2, 1))
        self.assertEqual(parse_date("07.02.2023"), date(2023, 2, 7))
        self.assertEqual(parse_date("7.2.2023"), date(2023, 2, 7))
        self.assertEqual(parse_date("7.2.2023", False), date(2023, 2, 7))
        self.assertEqual(parse_date("past"), date.min)
        self.assertEqual(parse_date("future"), date.max)
        self.assertEqual(parse_date("now"), date.today())
        self.assertEqual(parse_date("today"), date.today())
        with self.assertRaises(ValueError):
            parse_date("aaaa")

    def test_parse_timerange(self):
        (start, end) = parse_timerange("2010-2023")
        self.assertEqual(start, date(2010, 1, 1))
        self.assertEqual(end, date(2023, 12, 31))

        (start, end) = parse_timerange("6.10.2010-today")
        self.assertEqual(start, date(2010, 10, 6))
        self.assertEqual(end, date.today())

        (start, end) = parse_timerange("past-future")
        self.assertEqual(start, date.min)
        self.assertEqual(end, date.max)  # todo Sollte future ein Synonym für today sein?

        with self.assertRaises(ValueError):
            parse_timerange("2000-")  # todo Sollte das Okay sein?

        with self.assertRaises(ValueError):
            parse_timerange("-2000")  # todo Sollte das Okay sein?

        with self.assertRaises(AssertionError):
            parse_timerange("3000-2000")

    def test_list_density(self):
        l = list(range(10))

        l_same = reduce_list_density(l, 1)
        self.assertEqual(l_same, l)

        l_sub = reduce_list_density(l, 2)
        self.assertEqual(l_sub, [1, 3, 5, 7, 9])


        l = list(range(11))

        l_same = reduce_list_density(l, 1)
        self.assertEqual(l_same, l)

        l_sub = reduce_list_density(l, 2)
        self.assertEqual(l_sub, [0, 2, 4, 6, 8, 10])

    def test_parse_station(self):
        # Actually not in utils, but in dbmodels
        line1 = '19898 20240101 20241108             39     52.4970   13.2820 Berlin-Halensee                          BerlinFrei                                                                     '
        line2 = '19917 20240325 20241108            153     49.9321    8.0767 Schwabenheim a.d. Selz                   Rheinland-Pfalz                                                                                                   '
        line3 = '19897 20240101 20241112             37     52.5040   13.4550 Berlin-Friedrichshain-Nord               Berlin                           Frei                                                                     '

        station1 = dbmodels.Station()
        station1.build_from_text(line1)
        self.assertEqual(station1.id, 19898)
        self.assertEqual(station1.height, 39)
        self.assertEqual(station1.geo_lat, 52.4970)
        self.assertEqual(station1.geo_lon, 13.2820)
        self.assertEqual(station1.name, "Berlin-Halensee")
        self.assertEqual(station1.county, "Berlin")

        station2 = dbmodels.Station()
        station2.build_from_text(line2)
        self.assertEqual(station2.id, 19917)
        self.assertEqual(station2.height, 153)
        self.assertEqual(station2.geo_lat, 49.9321)
        self.assertEqual(station2.geo_lon, 8.0767)
        self.assertEqual(station2.name, "Schwabenheim a.d. Selz")
        self.assertEqual(station2.county, "Rheinland-Pfalz")

        station3 = dbmodels.Station()
        station3.build_from_text(line3)
        self.assertEqual(station3.id, 19897)
        self.assertEqual(station3.height, 37)
        self.assertEqual(station3.geo_lat, 52.5040)
        self.assertEqual(station3.geo_lon, 13.4550)
        self.assertEqual(station3.name, "Berlin-Friedrichshain-Nord")
        self.assertEqual(station3.county, "Berlin")

    def test_persistent_counter_not_existing(self):
        counter_file_name = pathlib.Path(".") / "test" / "fake_counter"
        counter_lockfile_name = counter_file_name.with_suffix(".lock")
        logging.debug(f"{counter_file_name=}")
        # Make sure that this file does not exist and do not complain if this file does not exist
        counter_file_name.unlink(True)
        pc = PersistentCounter(counter_file_name)
        try:
            self.assertEqual(counter_file_name.read_text("utf8"), "0\n")
            for n in range(1, 40):
                pc.increment()
                self.assertEqual(counter_file_name.read_text("utf8"), str(n) + "\n")
        finally:
            counter_file_name.unlink()  # Remove it again
            counter_lockfile_name.unlink()

    def test_persistent_counter_already_existing(self):
        counter_file_name = pathlib.Path(".") / "test" / "fake_counter"
        counter_lockfile_name = counter_file_name.with_suffix(".lock")
        logging.debug(f"{counter_file_name=} {counter_file_name.absolute()=}")
        counter_file_name.unlink(True)
        with counter_file_name.open("wt", encoding="utf8") as fake_counter_file:
            fake_counter_file.write("12")
        pc = PersistentCounter(counter_file_name)
        try:
            self.assertEqual(counter_file_name.read_text("utf8"), "12\n")
            for n in range(13, 40):
                pc.increment()
                self.assertEqual(counter_file_name.read_text("utf8"), str(n) + "\n")
        finally:
            counter_file_name.unlink()  # Remove it again
            counter_lockfile_name.unlink()


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
