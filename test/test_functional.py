import io
import unittest
from unittest import TestCase

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

import lib.dbutils
import lib.connection
from lib.plotcounty import PlotCounty


class TemperaturverlaufWebTest (TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_for_CSRF(self):
        """This test uses the running web server: It is called rather often, and we look for a CSRF error"""
        types = list(lib.dbutils.get_county_types().keys())
        counties = lib.dbutils.get_county_names()

        self.browser.get('http://localhost:5000')
        found_csrf = False
        csrf_round = 0
        for method_type in types[:5]:  # Nur die ersten paar
            for county in counties[:5]:  # dito
                csrf_round += 1
                # logging.error(f"In round {csrf_round}")
                Select(self.browser.find_element(By.ID, 'what')).select_by_value(method_type)
                Select(self.browser.find_element(By.ID, 'county')).select_by_value(county)
                self.browser.find_element(By.ID, 'submit').click()
                body = self.browser.find_element(By.TAG_NAME, 'body').get_attribute('innerHTML')
                # print(body)
                if 'CSRF' in body:
                    found_csrf = True
                    break
                self.assertIn(county, body)

        self.assertFalse(found_csrf, f"CSRF error in round {csrf_round}")

    def test_last_date(self):
        """This test uses the running database: It looks if last_date() returns something."""
        buffer = io.BytesIO()
        pc = PlotCounty(window=30, type_indicator="Lufttemperatur",
                        unit="°C",
                        counties=("Bayern",), format="svg",
                        base="year",
                        smoothed=False,
                        output_buffer=buffer, image_name="dummy")
        last_date = pc.last_date()
        self.assertEqual(len(last_date), 7, f"Got {last_date}")


if __name__ == '__main__':
    unittest.main()
