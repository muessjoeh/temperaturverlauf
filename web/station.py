import folium
import logging
from datetime import timedelta

from diskcache import Cache
from flask import Flask
import lib.dbutils
import lib.logger
from lib.connection import SessionServer
import lib.conf

configuration = lib.conf.Configuration()
log = logging.getLogger(__name__)
app = Flask(__name__)
configuration.use_default_configuration_file()
app.config["SECRET_KEY"] = configuration.get("secret")
assert configuration.get('secret') is not None
log.debug(f"The length of the secret key is {len(configuration.get('secret'))}")
cache = Cache('cache', size_limit=int(configuration.get("cache_size", '4_000_000')))
app.permanent_session_lifetime = timedelta(minutes=3)  # A session is only remembered for this time


@app.route("/stationmap")
def station_map():
    # todo Auf mehrere Dateien verteilen
    if 'stations' in cache:
        log.debug("Cache hit for 'stations''")
        html = cache['stations']
    else:
        m = folium.Map(location=[51.16, 10.44], zoom_start=7)  # Center of Germany
        with SessionServer() as session:
            temperature_type = lib.dbutils.get_station_mt_named("TT_TU")  # Lufttemperatur
            for station in lib.dbutils.get_all_stations():
                if lib.dbutils.measurements_exist(session, temperature_type, station):
                    # When there exist measurements for this station, link it and change its color
                    color = 'red'
                    popup = f'<a href="http://www.leo.org">{station.name}<a>'
                else:
                    color = 'blue'
                    popup = None
                folium.Marker(location=[station.geo_lat, station.geo_lon],
                              popup=popup,
                              tooltip=station.name, icon=folium.Icon(color=color)) \
                    .add_to(m)
        html = m._repr_html_()
        cache['stations'] = html
    return html


if __name__ == '__main__':
    app.run(port=5000, debug=True)
