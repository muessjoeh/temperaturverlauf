import base64
import io
import pathlib
import datetime
import sqlite3
from datetime import timedelta

import git
import requests
from flask import Flask, render_template, session
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField, ValidationError
import logging
from diskcache import Cache

# todo logger reparieren
import lib.conf
import lib.dbutils
import lib.logger
import lib.utils
from lib.plotcounty import PlotCounty

configuration = lib.conf.Configuration()
log = logging.getLogger(__name__)
app = Flask(__name__)
Bootstrap(app)
configuration.use_default_configuration_file()
cache_size = int(configuration.get("cache_size", '4_000_000'))
if cache_size != 0:
    cache = Cache('cache', size_limit=cache_size)  # initial default size
else:
    cache = None
    log.info("Using no cache")
app.config["SECRET_KEY"] = configuration.get("secret")
log.debug(f"Is the secret key there: {configuration.get('secret') is not None}")
app.permanent_session_lifetime = timedelta(minutes=5)  # A session is only remembered for this time


def check_if_possible(form, field):
    """Validate if two user page options are possible, eg 'Tropennächte/Winter' is not possible.
    if not, an exception is raised"""
    base_value = field.data
    what_value = form.what.data
    log.debug(f"In the check; base: {base_value}, what: {what_value}")
    if base_value != "year" and not lib.utils.are_seasons_possible(what_value):
        log.debug(f"Validation failed for {base_value}/{what_value}")
        raise ValidationError(f"'{what_value}' geht nur für das ganze Jahr")
    log.debug("Validation had no errors")


class CountyForm(FlaskForm):
    base = SelectField('', choices=[], validators=[check_if_possible])
    what = SelectField('', choices=[])
    county = SelectField('', choices=[])
    display_type = SelectField('', choices=(('Grafik_g', 'Grafik (nur geglättet)'),
                                            ('Grafik_gu', 'Grafik (geglättet+ungeglättet)'),
                                            ('Sound', 'Sound')),
                               default='Grafik_gu')
    submit = SubmitField('Los')

    def is_empty(self):
        if self.base.data is None or self.base.data == 'None' or \
                self.what.data is None or self.what.data == 'None' or \
                self.county.data is None or self.county.data == 'None' or \
                self.display_type is None or self.display_type.data == 'None':
            return True
        else:
            return False

    def store_in_session(self, session):
        session.permanent = True  # Wrongly named, it should read "obey_app_lifetime". By default, it is eternally
        session['base'] = self.base.data
        session['county'] = self.county.data
        session['what'] = self.what.data
        session['display_type'] = self.display_type.data

    def get_from_session(self, session):
        session.permanent = True
        # Take 'base' as a random sample to test for emptiness of the session
        if session.get('base') is None:
            return  # Do not retrieve empty values from the session
        self.base.data = session.get('base')
        self.county.data = session.get('county')
        self.what.data = session.get('what')
        self.display_type.data = session.get('display_type')
        
    def initialize_fields(self):
        # Alle Bundesländer, aber mit vernünftigen Namen. Beispielsweise 'Thüringen' anstatt 'Thueringen'
        self.county.choices = [(county_name, lib.utils.sanitize_string(county_name))
                               for county_name in lib.dbutils.get_county_names()]
        self.county.default = 'Deutschland'

        self.base.choices = [('spring', 'Frühling'),
                             ('summer', 'Sommer'),
                             ('autumn', 'Herbst'),
                             ('winter', 'Winter'),
                             ('year', 'Jahr')]
        self.base.default = 'year'

        # log.debug(f"Known types: {lib.list(dbutils.get_county_types().keys())}")
        self.what.choices = [(c, c) for c in lib.dbutils.get_county_types().keys()]
        self.what.default = 'Lufttemperatur'

    def __repr__(self):
        return f"{self.base.data}.{self.what.data}.{self.county.data}.{self.display_type.data}"


def as_encode_base64(binary):
    """Take any binary and encode it as a base64 encoded string"""
    base64_bytes = base64.b64encode(binary)
    return base64_bytes.decode("ascii")


@app.route('/', methods=["GET", "POST"])
def img_county():
    form = CountyForm()
    form.initialize_fields()
    counter_file = get_counter_file_name()
    pc = lib.utils.PersistentCounter(counter_file)
    pc.increment()
    
    if not form.validate_on_submit():
        log.debug("not validate_on_submit")
        form.process()
        form.store_in_session(session)
    else:
        log.debug("Validation on submit done")
        if form.is_empty():
            form.get_from_session(session)

    key = 'bundesland.' + str(form)
    if cache and key in cache:
        log.debug(f"Cache hit for {key}")
        content_in_base64 = cache[key]
    else:
        log.debug(f"Cache miss for {key} or no cache used")
        if form.display_type.data == 'Sound':
            log.debug(f"Creating MP3; form display_type is {form.display_type.data}")
            content_in_base64 = mp3_for_county(form)
        elif form.display_type.data == 'Grafik (nur geglättet)':
            log.debug(f"Creating SVG (only smoothed); form display_type is {form.display_type.data}")
            content_in_base64 = svg_for_county(form)
        else:
            log.debug(f"Creating SVG (both); form display_type is {form.display_type.data}")
            content_in_base64 = svg_for_county(form)
        if cache_size > 0:
            log.debug("Storing in the cache")
            cache[key] = content_in_base64  # store the result in the cache
        else:
            log.debug(f"No cache desired {cache}, so do not store there")
    page_html = render_template('bundesland.html',
                                content=content_in_base64,
                                form=form,
                                key=f'bundesland.{str(form)}')

    log.debug(f"Length of the encoded page: {len(page_html)}")
    return page_html


def get_counter_file_name():
    if pathlib.Path("cache").is_dir():
        return pathlib.Path("cache/counter")
    else:
        return pathlib.Path("counter")


@app.route("/counter")
def counter():
    with get_counter_file_name().open("rt", encoding="utf8") as counter_handle:
        return counter_handle.read()


@app.route("/status")
def status():
    # databases/mysql
    # system_image
    # webapps

    db_mysql = ask_pythonanywhere("databases/mysql")
    si = ask_pythonanywhere("system_image")
    wa = ask_pythonanywhere("webapps")
    cpu = ask_pythonanywhere("cpu")

    tz_string = datetime.datetime.now(datetime.timezone.utc).astimezone().tzname()
    now = str(datetime.datetime.now()) + " " + tz_string

    cached_items = []
    if pathlib.Path("cache/cache.db").is_file():
        # A cache file exists
        with sqlite3.Connection("cache/cache.db") as connection:
            cursor = connection.cursor()
            cursor.execute("select key from Cache")
            rows = cursor.fetchall()
            for row in rows:
                cached_items.append(row[0])
    else:
        cached_items = ("N/A",)

    with get_counter_file_name().open("rt", encoding="utf8") as counter_handle:
        repo = git.Repo(configuration.get('source_directory'))
        render = render_template('status.txt',
                                 counter=counter_handle.read().strip(),
                                 branch=repo.active_branch.name,
                                 db_mysql=db_mysql,
                                 si=si,
                                 wa=wa,
                                 now=now,
                                 cached=", ".join(cached_items),
                                 cpu=cpu)
        return render


def ask_pythonanywhere(what: str) -> str:
    username = 'temperaturverlauf'
    token = configuration.get("token")

    url = f"https://www.pythonanywhere.com/api/v0/user/{username}/{what}/"
    token_header = f"Token {token}"
    log.debug(f"{url=}")
    log.debug(f"{token_header=}")

    try:
        response = requests.get(url, headers={'Authorization': token_header})
        if response.status_code == 200:
            return str(response.content)
        else:
            return f'Got unexpected status code {response.status_code} for {what}: {response.content}'
    except:
        return "No connection"


def x_for_county(county_form, the_format):
    buffer = io.BytesIO()
    smoothed = county_form.display_type.data == "Grafik_g"
    pc = PlotCounty(window=30, type_indicator=county_form.what.data,
                    unit=lib.dbutils.get_county_types()[county_form.what.data],
                    counties=(county_form.county.data,), format=the_format,
                    base=county_form.base.data,
                    smoothed=smoothed,
                    output_buffer=buffer,
                    image_name="dummy",
                    point_density=2 if smoothed else 1)
    pc.do_it()
    # log.info(f"Length of the content: {len(buffer)}; its type: {type(buffer)}")
    buffer_in_base64 = as_encode_base64(buffer.getvalue())
    pc.close()
    return buffer_in_base64


def svg_for_county(county_form):
    return x_for_county(county_form, "svg")


def mp3_for_county(county_form):
    return x_for_county(county_form, "mp3")


if __name__ == '__main__':
    app.run(port=5000, debug=True)
