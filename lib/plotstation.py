import logging
import sys
import time
from abc import ABC, abstractmethod

import psutil
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit
from sqlalchemy import and_, extract, func
from sqlalchemy.exc import ProgrammingError
from tqdm import tqdm

from lib.connection import SessionServer
from lib.dbmodels import StationMeasurement, MeasurementTypeOfStation
from lib.utils import split_into_chunks, reduce_list, dict_sequence, linear, \
    completely_in_year, seasons, translation_en_de

log = logging.getLogger(__name__)


class PlotStation(ABC):
    """Plots of data of a certain station, for example Augsburg"""

    def __init__(self, base, diagram_type, measurement_type, station, image_name):
        self.all_data = {}
        self.measured_data = {}
        self.base = base
        self.diagram_type = diagram_type
        self.measurement_type = measurement_type
        self.station = station
        self.image_name = image_name
        log.debug(f"Station: {self.station}")

    seasonranges = {"spring": (3, 4, 5), "summer": (6, 7, 8),
                    "autumn": (9, 10, 11), "winter": (12, 1, 2),
                    "year": range(1, 12 + 1)}

    def is_necessary_to_sum_measurements(self):
        return self.measurement_type.name == "R1"

    def fetch_data(self):
        # DB -> self.all_data
        log.debug("Fetch all needed entries out of the database")

        total_entries = 0

        group_range = self.get_range_of_group()
        log.debug(f"Used range: {group_range}")

        for examined_time in tqdm(group_range):
            log.debug(f"Fetching data for {self.get_name()} {examined_time}")

            try:
                all_data = self.fetch_from_db(current_time_range=examined_time)
                if len(all_data) != 0:
                    log.debug(f"First measurement: {all_data[0]}")
                else:
                    log.debug("No elements found")
                # all_data = reduce_list(all_data)
                number_of_entries_in_this_range = len(all_data)
                total_entries += number_of_entries_in_this_range
                log.debug(f"Number of entries in this range {number_of_entries_in_this_range}")
                # log.debug(f"all_data before split_into_chunks: {all_data}")
                # Cluster the entries
                log.debug(f"splitting the measurements with '{self.measurement_type}' as payload name")
                all_data = split_into_chunks(all_data, self.measurement_type.name,
                                             self.duration_seconds(),
                                             sum_up_values=self.is_necessary_to_sum_measurements())
                log.debug(f"They were split to {len(all_data)} entries")
                # log.debug(f"all_data after split_into_chunks: {all_data}")
                all_data = reduce_list(all_data)
                # log.debug(f"all_data after reduce: {all_data}")
                self.all_data[examined_time] = all_data
            except ProgrammingError:
                raise LookupError("An error occurred while trying to read the database. "
                                  "Have you run persist.py?")

            if number_of_entries_in_this_range == 0:  # No entries found in this time range
                log.warning(f"No entries found in {examined_time}")
                del self.all_data[examined_time]
            # self.prepare_data(examined_time)

        # When no entry at all is found, there is something rotten
        if total_entries == 0:
            raise LookupError("No data found. Have you run persist.py "
                              "and stored the correct measurement files for this station?")

        log.debug("Done with fetching")
        # log.debug(f"all_data: {all_data}")
        log.debug(f"self.all_data: {self.all_data}")

    def months_of_season(self):
        # By default, fetch the whole year
        months = range(1, 12 + 1)
        if self.base is not None:
            months = seasons[self.base]
        return months

    @abstractmethod
    def duration_seconds(self):
        pass

    @abstractmethod
    def get_range_of_group(self):
        pass

    @abstractmethod
    def fetch_from_db(self, current_time_range=None):
        pass

    @abstractmethod
    def get_name(self):
        pass

    @abstractmethod
    def is_barplot_desired(self):
        pass

    def plot(self):
        column_doc, unit = self.plot_boilerplate(self.measurement_type)
        self.plot_diagram(column_doc, unit)
        if self.image_name is None:
            plt.show()
        else:
            if self.image_name == "-":
                plt.savefig(sys.stdout.buffer)
            else:
                plt.savefig(self.image_name)

    def plot_boilerplate(self, type):
        fig, ax = plt.subplots()
        ax.set_xlabel(translation_en_de[self.get_name()])
        unit = type.unit
        column_doc = type.pretty_description()
        ax.set_ylabel(f"Durchschnitt {column_doc} im {translation_en_de[self.get_name()]} "
                      f"[{unit}]")
        plt.title(f"{column_doc} in {self.station.name}")
        plt.grid(which='major', color='#666666', linestyle='-')
        plt.grid(which='minor', color='#888888', linestyle='-', alpha=0.4)
        plt.minorticks_on()
        # plt.yscale('log')
        ax.xaxis.set_major_locator(plt.MaxNLocator(6))  # Needs to be an even number
        return column_doc, unit

    def plot_diagram(self, column_doc, unit):
        # Put the measured data into a bar, box or violin plot
        fit_year = 0
        if self.get_name() == "year":
            # When a year plot is wanted. Done because the massive temperature rise begins 1980
            fit_year = 1980
        filtered_data = {year: measurements for year, measurements in self.all_data.items()
                         if year >= fit_year}
        filtered_keys = list(filtered_data.keys())
        filtered_values = list(filtered_data.values())
        keys = list(self.all_data.keys())
        values = list(self.all_data.values())
        log.debug(f"fit_year: {fit_year} for group {self.get_name()}")
        log.debug(f"Number of keys: {len(keys)}, values: {len(values)}")
        log.debug(f"keys: {keys}")
        log.debug(f"values: {values}")
        log.debug(f"Number of filtered keys: {len(filtered_keys)}, "
                  f"filtered values: {len(filtered_values)}")
        log.debug(f"filtered keys: {filtered_keys}; type: {type(filtered_keys[0])}")
        log.debug(f"filtered values: {filtered_values}")
        len_values = 0
        for value in filtered_values:
            log.debug(f"  Number of elements in this value list is {len(value)},"
                      f" the average is {sum(value) / len(value)}")
            len_values += len(value)
        log.debug(f"Total number of filtered_values: {len_values}, "
                  f"that is on average {len_values / len(keys)} per {self.get_name()}")

        log.debug(f"Desired grouping is {self.get_name()}")
        if self.get_name() == "year":
            self.plot_linear_fit(column_doc, filtered_data, unit, fit_year,
                                 len(keys) != len(filtered_keys))
        else:
            plt.legend([column_doc], loc="upper left")
        bins = 5
        if len(keys) % 2 != 0:
            bins = 6  # When there is an odd number of years, take an even number of bins
        log.debug(f"Number of bins: {bins}")
        plt.locator_params(nbins=bins)

        if self.is_barplot_desired():
            log.debug(f"barplot; original filtered_values: length: {len(values)}, {filtered_values}")
            log.debug(f"reduced filtered_values: {len(reduce_list(values))}, {reduce_list(values)}")
            plt.bar(filtered_keys, reduce_list(filtered_values), color="b")
        elif self.diagram_type == "violin":
            log.debug("Violin plot")
            plt.violinplot(values, positions=keys, showmedians=True)
        else:
            log.debug("box plot")
            plt.boxplot(values, positions=keys)

    def plot_linear_fit(self, column_doc, data, unit, fit_year, truncated):
        # Add a linear fit
        x, y = dict_sequence(data)
        best_vals, covar = curve_fit(linear, x, y)
        y_fitted = [linear(x, best_vals[0], best_vals[1]) for x in data.keys()]
        log.debug(f"Keys: {data.keys()}")
        log.debug(f"Fitted Y values: {y_fitted}")
        plt.plot(list(data.keys()), y_fitted, color="r")
        since_string = f" (ab {fit_year})"
        plt.legend([f"Lineare Schätzung. Steigung "
                    f"{best_vals[0]:.2f}{unit} pro Jahr{since_string if truncated else ''}",
                    column_doc],
                   loc="upper left")

    def do_it(self):
        memory_at_begin = str(psutil.virtual_memory())
        t1 = time.monotonic()

        log.debug(f"duration connect: {time.monotonic() - t1:.1f}s")
        t2 = time.monotonic()
        self.fetch_data()
        log.debug(f"duration fetch: {time.monotonic() - t2:.1f}s")
        t4 = time.monotonic()
        self.plot()
        log.debug(f"duration plot: {time.monotonic() - t4:.1f}s")
        log.debug(f"Memory footprint at begin: {memory_at_begin}")
        log.debug(f"Memory footprint at end: {str(psutil.virtual_memory())}")


class PlotStationYear(PlotStation):
    """Whole year plots of data of a certain station, for example Augsburg"""

    def duration_seconds(self):
        return 60 * 60 * 60 * 366  # A bit more than one year

    def get_range_of_group(self):
        with SessionServer() as session:
            type_db = session.query(MeasurementTypeOfStation).\
                filter(MeasurementTypeOfStation.name == self.measurement_type.name).\
                first()
            min_date = session.query(func.min(StationMeasurement.date)). \
                filter(and_((StationMeasurement.measurementtype_id == type_db.id),
                            StationMeasurement.station_id == self.station.id)).scalar()
            max_date = session.query(func.max(StationMeasurement.date)). \
                filter(and_((StationMeasurement.measurementtype_id == type_db.id),
                            StationMeasurement.station_id == self.station.id)).scalar()
            session.commit()
        log.debug(f"min: {min_date}, max: {max_date}")
        log.debug(f"self.measurementtype: {self.measurement_type};"
                  f" Station ID: {self.station.id}; self.base: {self.base}")
        if (min_date is None) or (max_date is None):
            raise RuntimeError("No data found. Have you run fetch.py and persist.py?")
        min_year = min_date.year
        max_year = max_date.year
        if not completely_in_year(min_date.month, self.base):  # The first year may be truncated
            log.debug("Incomplete year at beginning found")
            min_year += 1
        if not completely_in_year(max_date.month, self.base):  # The last year may be truncated
            log.debug("Incomplete year at beginning found")
            max_year -= 1
        log.debug(f"min_year: {min_year}, max_year: {max_year}")
        return range(min_year, max_year + 1)

    def fetch_from_db(self, current_time_range=None):
        log.debug(f"year.fetch_from_db(). current_time_range: {current_time_range}; "
                  f"type: {self.measurement_type}; Station: {self.station}")
        with SessionServer() as session:
            type_db = session.query(MeasurementTypeOfStation).\
            filter(MeasurementTypeOfStation.name == self.measurement_type.name).\
            first()
            all_data = session.query(StationMeasurement.date, StationMeasurement.value). \
                filter(
                and_(
                    and_(
                        extract("year", StationMeasurement.date) == current_time_range,
                        extract("month", StationMeasurement.date).in_(self.seasonranges[self.base])),
                    StationMeasurement.measurementtype_id == type_db.id),
                StationMeasurement.station_id == self.station.id).\
                order_by(StationMeasurement.date). \
                all()
            session.commit()
        return all_data

    def get_name(self):
        return "year"

    def is_barplot_desired(self):
        return self.measurement_type.name == "R1"


class PlotStationHour(PlotStation):
    """Hour plots of data of a certain station, for example Augsburg"""

    def duration_seconds(self):
        return 60 * 60

    def get_range_of_group(self):
        return range(0, 23 + 1)

    def fetch_from_db(self, current_time_range=None):
        log.debug(f"Fetching hour {current_time_range}")
        with SessionServer() as session:
            type_db = session.query(MeasurementTypeOfStation). \
                filter(MeasurementTypeOfStation.name == self.measurement_type.name). \
                first()
            all_data = session.query(StationMeasurement.date, StationMeasurement.value). \
                filter(
                and_(
                    and_(
                        extract("hour", StationMeasurement.date) == current_time_range,
                        extract("month", StationMeasurement.date).
                            in_(self.seasonranges[self.base])),
                    StationMeasurement.measurementtype_id == type_db.id),
                StationMeasurement.station_id == self.station.id). \
                order_by(StationMeasurement.date). \
                all()
            session.commit()
        return all_data

    def get_name(self):
        return "hour"

    def is_barplot_desired(self):
        return False


class PlotStationMonth(PlotStation):
    """Month plots of data of a certain station, for example Augsburg"""

    def duration_seconds(self):
        return 60 * 60 * 24 * 32  # A bit more than one month

    def get_range_of_group(self):
        return range(1, 12 + 1)

    def fetch_from_db(self, current_time_range=None):
        log.debug(f"month.fetch_from_db(). current_time_range: {current_time_range}; "
                  f"type: {self.measurement_type}; Station: {self.station}")
        with SessionServer() as session:
            type_db = session.query(MeasurementTypeOfStation). \
                filter(MeasurementTypeOfStation.name == self.measurement_type.name). \
                first()
            all_data = session.query(StationMeasurement.date, StationMeasurement.value). \
                filter(
                and_(
                    extract("month", StationMeasurement.date) == current_time_range,
                    StationMeasurement.measurementtype_id == type_db.id),
                StationMeasurement.station_id == self.station.id).\
                order_by(StationMeasurement.date). \
                all()
            session.commit()
        return all_data

    def get_name(self):
        return "month"

    def is_barplot_desired(self):
        return False


if __name__ == '__main__':
    print("Cannot call this module directly")
