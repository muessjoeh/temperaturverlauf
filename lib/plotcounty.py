import logging
import sys
import time

import matplotlib
import numpy as np
import pydub
import scipy.signal
from scipy.io import wavfile
from matplotlib import pyplot as plt
from sqlalchemy import and_, func
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import joinedload

import lib.conf
from lib.connection import SessionServer
from lib.dbmodels import MeasurementTypeOfCounty, CountyMeasurement, County
from lib.utils import rolling_mean, has_sufficient_entries, seasons, Aggregator,\
    translation_en_de, sanitize_string, \
    map_range

log = logging.getLogger(__name__)


class PlotCounty:
    """Various average data of each Bundesland 1881-2020"""

    def __init__(self, window, type_indicator, unit, counties, base, smoothed,
                 image_name=None, format=None, output_buffer=None, point_density=2):
        self.county_smoothed_measurements_values = {}
        self.county_smoothed_measurements_years = {}
        self.configuration = lib.conf.Configuration()
        self.configuration.use_default_configuration_file()
        self.base = base
        log.debug(f"base: %s", base)
        try:
            self.base_months = (int(base), )
            self.base = f"month {base}"
        except ValueError:  # Cannot convert it => it is a non-number string, so smth like "summer"
            self.base_months = seasons[base]
        self.last_base_month = self.base_months[-1]
        if self.last_base_month == -1:
            self.last_base_month = self.base_months[-2]
        log.debug("Last month of this base: %d", self.last_base_month)
        log.debug("counties: %s", counties)
        self.desired_counties = counties
        self.smoothed = smoothed
        self.window = window
        self.type_indicator = type_indicator
        self.unit = unit
        self.image_name = image_name
        self.format = format
        self.output_buffer = output_buffer
        self.counties = []
        self.county_measurements_years = {}
        self.county_measurements_values = {}
        self.point_density = point_density
        for c in self.desired_counties:
            self.counties.append(c)
        with SessionServer() as session:
            self.type = session.query(MeasurementTypeOfCounty). \
                filter(MeasurementTypeOfCounty.name == self.type_indicator). \
                first()
        log.debug(f"Type indicator: {self.type_indicator}; Type: {self.type}")
        log.debug(f"Counties: {self.counties}")

    def fetch_data(self):
        log.debug("Fetch all entries")
        try:
            with SessionServer() as session:
                for county in self.counties:
                    county_db = session.query(County).filter_by(name=county).first()
                    session.commit()
                    log.debug(f"County from DB: {county_db}")
                    type_db = session.query(MeasurementTypeOfCounty).\
                        filter_by(name=self.type_indicator).first()
                    session.commit()
                    log.debug(f"Type from DB: {type_db}")
                    query_start = time.time()
                    measurements = session. \
                        query(CountyMeasurement). \
                        options(joinedload('*')). \
                        filter(and_(and_(CountyMeasurement.month.in_(self.base_months),
                                         CountyMeasurement.type == type_db),
                                    CountyMeasurement.county == county_db)).\
                        order_by(CountyMeasurement.year.asc(), CountyMeasurement.month.asc()).\
                        all()
                    session.commit()  # To prevent a rollback
                    log.debug(f"It took {time.time() - query_start} to fetch from the DB")
                    if len(measurements) == 0:
                        raise LookupError("No data found. Have you run fetch.py and persist.py?")
                    log.debug(f"{len(measurements)} entries found")
                    while measurements[-1].month != -1 and measurements[-1].month != self.last_base_month:
                        del measurements[-1]
                        log.debug(f"Now there are {len(measurements)} measurements left, the last one is {measurements[-1]}")
                    log.debug("Finished cleaning the list from the end")

                    # todo hier passieren die vielen selects
                    query_start = time.time()
                    aggregated_measurements = {}
                    for measurement in measurements:
                        # Put all measurements of that county in an aggregate
                        # log.debug(f"Looking at measurement {measurement}")
                        aggregated_measurement =\
                            aggregated_measurements.get(measurement.year,
                                                        Aggregator.aggregator_factory(
                                                            self.type_indicator))
                        aggregated_measurements[measurement.year] = aggregated_measurement
                        aggregated_measurements[measurement.year].collect(measurement.value)
                        # log.debug(f"Result of collecting: {len(aggregated_measurements)} fields: "
                        #      f"{','.join(map(str, aggregated_measurements.values()))}")
                    log.debug("End of for loop through measurements to aggregate them")
                    log.debug(f"It took {time.time() - query_start}s to loop through these entries")

                    self.county_measurements_years[county] = []
                    self.county_measurements_values[county] = []
                    for year, aggregated_value in aggregated_measurements.items():
                        if has_sufficient_entries(aggregated_value.get_length(), self.base_months):
                            self.county_measurements_years[county].append(year)
                            self.county_measurements_values[county].\
                                append(aggregated_value.aggregate())
                        else:
                            log.debug(f"Not enough entries for {year}. "
                                      f"Expected {len(self.base_months)}, "
                                      f"but got {aggregated_value.get_length()}")
                    # log.debug(f"years: {self.county_measurements_years}")
                    # log.debug(f"values: {self.county_measurements_values}")
                    session.commit()
                    log.debug("End of for loop through aggregated_measurements")
        except OperationalError:
            raise LookupError("No data found. Have you run fetch.py and persist.py?")

    def prepare_data(self):
        for county in self.county_measurements_years.keys():
            self.county_smoothed_measurements_years[county] =\
                self.county_measurements_years[county][:]
            if self.window == 1:
                self.county_smoothed_measurements_values[county] = self.county_measurements_values[county]
            else:
                window = lib.utils.nearest_odd_number(self.window)
                self.county_smoothed_measurements_values[county] =\
                    scipy.signal.savgol_filter(self.county_measurements_values[county], window, 2)

    def plot_boilerplate(self):
        """Plot axes, grid etc"""
        if self.image_name == "dummy":
            matplotlib.use('agg')  # prevent a crash when there is no cache
        fig, ax = plt.subplots()
        ax.set_xlabel("Jahr")
        ax.set_ylabel(f"Durchschnitt im {translation_en_de[self.base]} [{self.unit}]")
        if len(self.counties) != 1:
            county = "verschiedenen Bundesländern"
        else:
            county = self.counties[0]
        if self.window == 1:
            graph_title = f"{self.type_indicator} in {sanitize_string(county)}"
        else:
            graph_title = f"{self.type_indicator} in {sanitize_string(county)} (Glättung über {self.window} Jahre)"
        plt.title(graph_title)
        plt.grid(which='major', color='#666666', linestyle='-', alpha=0.3)
        plt.grid(which='minor', color='#888888', linestyle='-', alpha=0.05)
        plt.minorticks_on()

    def plot(self):
        # log.debug(f"self.county_measurements_years: {self.county_measurements_years}")
        # log.debug(f"self.county_measurements_values: {self.county_measurements_values}")
        self.plot_boilerplate()
        log.debug(f"In plot(): Counties: {self.counties}")
        for county in self.county_measurements_years.keys():
            log.debug(f"In plot(): County: {county}")
            # Plot unsmoothed data only when there is only 1 county, otherwise it is too confusing
            color = None
            if len(self.counties) == 1 and not self.smoothed:
                color = 'b'
                plt.plot(lib.utils.reduce_list_density(self.county_measurements_years[county], self.point_density),
                         lib.utils.reduce_list_density(self.county_measurements_values[county], self.point_density),
                         color=color,
                         label=f"{county} (ungeglättet)",
                         alpha=0.2)
            log.debug(f"Smoothed years: {self.county_smoothed_measurements_years}")
            log.debug(f"Smoothed values: {self.county_smoothed_measurements_values}")
            plt.plot(lib.utils.reduce_list_density(self.county_smoothed_measurements_years[county], self.point_density),
                     lib.utils.reduce_list_density(self.county_smoothed_measurements_values[county], self.point_density),
                     color=color,
                     label=sanitize_string(county))
        if len(self.counties) != 1:
            plt.legend(loc="upper left", fancybox=True, framealpha=0.65)
        else:
            # todo Unabhängig von der Anzahl?
            ax = plt.gca()
            (bottom, top) = ax.get_ylim()
            (left, right) = ax.get_xlim()
            plt.text(right, bottom,
                     f"Stand {self.last_date()}",
                     horizontalalignment='right',
                     verticalalignment='bottom',
                     size=6)
        if self.image_name is None:
            plt.show()
        else:
            log.debug(f"format: {self.format}")
            if self.image_name.endswith(".wav") or (self.format is not None and self.format.lower() == "wav"):
                log.debug("Create WAV output")
                self.create_wav()
            elif self.image_name.endswith(".mp3") or (self.format is not None and self.format.lower() == "mp3"):
                log.debug("Create MP3 output")
                self.create_mp3()
            elif self.output_buffer is not None:
                log.debug(f"An output buffer of the type {type(self.output_buffer)} is given")
                plt.savefig(self.output_buffer, format=self.format)
            elif self.image_name == "-":
                plt.savefig(sys.stdout.buffer)
            elif len(self.image_name) == 5 and self.image_name.startswith("-."):  # sth like '-.svg'
                plt.savefig(sys.stdout.buffer, format=self.image_name[-3:])

            else:
                plt.savefig(self.image_name)

    def close(self):
        plt.close('all')

    def last_date(self):
        if len(self.counties) != 1:
            return "only one county"
        else:
            county_name = self.counties[0]
        with SessionServer() as session:
            county_id = session.query(County.id).filter(County.name == county_name).first()[0]
            session.commit()
            log.debug(f"county_id: {county_id}, measurementtype_id: {self.type.id}")
            year = session.query(func.max(CountyMeasurement.year)).\
                filter(and_(CountyMeasurement.measurementtype_id == self.type.id,\
                            CountyMeasurement.county_id == county_id)).\
                first()[0]
            session.commit()
            log.debug(f"last_date year is {year}")
            month = session.query(func.max(CountyMeasurement.month)).\
                filter(and_(CountyMeasurement.measurementtype_id == self.type.id,\
                            and_(CountyMeasurement.county_id == county_id), CountyMeasurement.year == year)).\
                first()[0]
            session.commit()
            log.debug(f"last_date month is {month}")
        if month is None or year is year is None:
            log.debug(f"year: {year}, month: {month}")
            return "unbekannt"
        elif month != -1:
            return f"{month:02d}/{year}"
        else:
            return f"{year}"

    @staticmethod
    def create_sound_snippet(frequency, duration, sample_rate):
        # Try to fit the duration into a multiple of 2pi
        tau = 2 * np.pi
        new_duration = round(duration * frequency / tau) * tau / frequency
        x_values = np.linspace(0, new_duration * round(tau), int(sample_rate * new_duration))
        y_values = np.sin(frequency * x_values)
        return y_values

    def create_sound(self):
        if len(self.counties) != 1:
            raise ValueError(f"Need exactly one county: {self.counties}")
        county = self.counties[0]
        # self.county_smoothed_measurements_years[county]
        measurement_range = (min(self.county_smoothed_measurements_values[county]),
                             max(self.county_smoothed_measurements_values[county]))
        scaled_measurements = [map_range(measurement_range, m)
                               for m in self.county_smoothed_measurements_values[county]
                               ]
        log.debug(f"range: {measurement_range}")
        log.debug(f"values: {self.county_measurements_values[county]}")
        log.debug(f"scaled: {scaled_measurements}")
        sound = np.arange(0)  # Start with nothing
        for measurement in scaled_measurements:
            sound = np.append(sound, self.create_sound_snippet(measurement,
                                                               self.configuration.get("tone_duration"),
                                                               self.configuration.get("sample_rate")))
        return sound

    def create_wav(self):
        sound = self.create_sound()
        if self.output_buffer is not None:
            wavfile.write(self.output_buffer, int(self.configuration.get("sample_rate")),
                          np.int16(sound * 32767))
        else:
            wavfile.write(self.image_name, int(self.configuration.get("sample_rate")),
                          np.int16(sound * 32767))

    def create_mp3(self):
        sound = self.create_sound()
        sound_int = np.int16(sound * 32767)
        song = pydub.AudioSegment(sound_int.tobytes(),
                                  frame_rate=self.configuration.get("sample_rate"),
                                  sample_width=2, channels=1)
        if self.output_buffer is not None:
            song.export(self.output_buffer, format="mp3",
                        bitrate=self.configuration.get("mp3_bitrate"))
        else:
            song.export(self.image_name, format="mp3",
                        bitrate=self.configuration.get("mp3_bitrate"))

    def do_it(self):
        t2 = time.monotonic()
        self.fetch_data()
        log.debug(f"duration fetch: {time.monotonic() - t2:.1f}s")
        t3 = time.monotonic()
        self.prepare_data()
        log.debug(f"duration prepare: {time.monotonic() - t3:.1f}s")
        t4 = time.monotonic()
        self.plot()
        log.debug(f"duration plot: {time.monotonic() - t4:.1f}s")


if __name__ == '__main__':
    print("Cannot call this module directly")
