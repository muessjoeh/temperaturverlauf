import logging

from lib.conf import Configuration


if __name__ == '__main__':
    raise RuntimeError("Cannot call this module directly")

configuration = Configuration()
configuration.use_default_configuration_file()
log_file_name = configuration.get("log_filename", "/dev/stderr")

configuration_log_level = str(configuration.get("log_level", "warning")).lower()
log_level = logging.WARNING
if configuration_log_level == 'debug':
    log_level = logging.DEBUG
elif configuration_log_level != 'warning' and configuration_log_level != 'warn':
    raise RuntimeError(f"Unknown log level {configuration.get('log_level')}")

logging.basicConfig(level=log_level,
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    filename=log_file_name)

# Overwhelmingly verbose matplotlib logger
logging.getLogger('matplotlib').setLevel(logging.WARN)
logging.getLogger('PIL.PngImagePlugin').setLevel(logging.WARN)
