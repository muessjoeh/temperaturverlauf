from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import lib.conf
from lib.dbmodels import Base


class SessionServer:
    def __init__(self):
        self.conf = lib.conf.Configuration()
        self.conf.use_default_configuration_file()
        engine = create_engine(self.conf.get("database_url"),
                               echo=self.conf.get('echo_sql_commands'))
        Base.metadata.create_all(engine)
        self.Session = sessionmaker(bind=engine)
        self.Session.configure()
        self.session_counter = 0

    def __enter__(self):
        self.session_counter += 1
        self.session = self.Session()
        return self.session

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.session_counter -= 1
        self.session.flush()
        self.session.close()


if __name__ == '__main__':
    print("Cannot call this module directly")
