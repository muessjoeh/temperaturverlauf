from yaml import load, Loader

import lib.utils


class Configuration:
    def __init__(self):
        self._configuration = {}

    def is_empty(self):
        return len(self._configuration) == 0

    def get(self, key, default=None):
        value = self._configuration.get(key, default)
        if value is None:
            value = ()
        return value

    def add_file(self, configuration_filename, configuration_dict=None):
        # A default parameter with 'self' did not work, so use 'None' instead
        if configuration_dict is None:
            configuration_dict = self._configuration
        real_configuration_filename = lib.utils.find_filename_recursively(configuration_filename)
        if real_configuration_filename is None:
            raise FileNotFoundError(f"Did not find the configuration file '{configuration_filename}'")
        # print(real_configuration_filename)
        try:
            with open(real_configuration_filename, "r") as configuration_file:
                content = load(configuration_file, Loader=Loader)
                for key, value in content.items():
                    configuration_dict[key] = value
        except FileNotFoundError:
            raise FileNotFoundError(f"Did not find '{configuration_filename}'. "
                                    f"Did you copy the corresponding *_sample file as a template?")

    def use_default_configuration_file(self):
        self.add_file("configuration.yaml", self._configuration)


if __name__ == '__main__':
    print("Cannot call this module directly")
