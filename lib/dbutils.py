"""Utility functions in the database context"""

from sqlalchemy import and_

import lib.connection
import lib.dbmodels


def get_county_names():
    """Return a list of all county names"""
    with lib.connection.SessionServer() as session:
        counties = session.query(lib.dbmodels.County).order_by(lib.dbmodels.County.name).all()
    return [county.name for county in counties]


def get_all_stations():
    """Return a list of all the stations as objects"""
    with lib.connection.SessionServer() as session:
        return session.query(lib.dbmodels.Station).all()


def get_station_named(name):
    """Return the station having a certain name or None"""
    with lib.connection.SessionServer() as session:
        return session.query(lib.dbmodels.Station).filter(lib.dbmodels.Station.name == name).first()


def get_station_mt_named(name):
    """Return the station measurement type having a certain name or None"""
    with lib.connection.SessionServer() as session:
        return session.query(lib.dbmodels.MeasurementTypeOfStation)\
            .filter(lib.dbmodels.MeasurementTypeOfStation.name == name)\
            .first()


def get_all_station_mt():
    """Return a list of all the station measurement types as objects"""
    with lib.connection.SessionServer() as session:
        return session.query(lib.dbmodels.MeasurementTypeOfStation).all()


def get_station_names_similar_to(name):
    """Return all stations that contain a specific name as a substring"""
    with lib.connection.SessionServer() as session:
        return session.query(lib.dbmodels.Station).filter(
            lib.dbmodels.Station.name.ilike(f"%{name}%")).all()


def get_county_types():
    """Return a dict (name:unit) of all stored measurement types for the counties"""
    with lib.connection.SessionServer() as session:
        ctypes = session.query(lib.dbmodels.MeasurementTypeOfCounty).all()
    return {t.name:t.unit for t in ctypes}


def measurements_exist(session, measurement_type, station):
    """Are there any measurements of a specific type for a station?
    Both the type and the station are objects"""
    return session.query(lib.dbmodels.StationMeasurement.station_id). \
               filter(and_( \
        (lib.dbmodels.StationMeasurement.station_id == station.id),
        (lib.dbmodels.StationMeasurement.measurementtype_id == measurement_type.id) \
        )). \
               first() is not None


if __name__ == '__main__':
    print("Cannot call this module directly")
