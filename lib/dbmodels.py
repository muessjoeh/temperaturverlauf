import logging

from sqlalchemy import Column, DateTime, Integer, Sequence, Numeric, ForeignKey, UniqueConstraint, String,\
    SmallInteger, Index
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

import lib.utils

Base = declarative_base()


class Station(Base):
    __tablename__ = "station"

    id = Column(SmallInteger, Sequence('station_id_seq'), primary_key=True)
    measurements = relationship('StationMeasurement', backref='station')
    # date_from = Column(Date)
    # date_to = Column(Date)
    height = Column(Integer)
    geo_lat = Column(Numeric(asdecimal=False, precision=10, scale=6))
    geo_lon = Column(Numeric(asdecimal=False, precision=10, scale=6))
    name = Column(String(50), nullable=False)
    county = Column(String(50), nullable=False)
    description = Column(String(100))

    def build_from_text(self, line: str):
        line_words = line.strip().split()
        logging.debug(f"{len(line_words)=}")

        self.id = int(line_words.pop(0))
        logging.debug(f"{self.id=}")
        _ = line_words.pop(0)  # Ignore von_datum
        _ = line_words.pop(0)  # Ignore bis_datum
        self.height = int(line_words.pop(0))
        logging.debug(f"{self.height=}")
        self.geo_lat = float(line_words.pop(0))
        self.geo_lon = float(line_words.pop(0))
        self.county = line_words.pop(-1)
        if self.county == "Frei":  # Wenn die Zeile mit "Frei" endet, nimm das nächste
            self.county = line_words.pop(-1)
        logging.debug(f"{self.county=}")
        self.name = " ".join(line_words)  # Der Rest ist der Name
        logging.debug(f"{self.name=}")
        if self.county.endswith("Frei"):
            logging.debug(f"{self.county=} ist mit Frei infiziert")
            self.county = self.county[:-4]
            logging.debug(f"Jetzt hoffentlich nicht mehr: '{self.county=}'")


        # self.id = int(line[0:5].strip())
        # self.height = int(line[30:38].strip())
        # self.geo_lat = float(line[42:50].strip())
        # self.geo_lon = float(line[52:60].strip())
        # self.name = line[61:101].strip()
        # self.county = line[102:-1].strip()

    def build_from_dict(self, dictionary):
        self.id = dictionary["Stations_id"]
        self.name = dictionary["Stationsname"]
        self.height = dictionary["Stationshoehe"]
        self.geo_lat = dictionary["geograph.Breite"]
        self.geo_lon = dictionary["geograph.Laenge"]
        self.county = dictionary["Bundesland"]
        self.description = dictionary["Naturraum"]

    def __repr__(self):
        return f"{self.name} ({self.county}, {self.id})"


class StationMeasurement(Base):
    __tablename__ = "stationmeasurement"
    __table_args__ = (UniqueConstraint('date', 'measurementtype_id', 'station_id', name='stationmeasurement_std_uc'),
                      Index('type_date_idx', 'station_id', 'measurementtype_id', 'date'))

    id = Column(Integer, Sequence('stationmeasurement_id_seq'), primary_key=True)
    station_id = Column(SmallInteger, ForeignKey("station.id"), nullable=False)
    measurementtype_id = Column(SmallInteger, ForeignKey("measurementtype_station.id"), nullable=False)
    date = Column(DateTime, nullable=False)
    quality = Column(SmallInteger, nullable=False)
    value = Column(Numeric(asdecimal=False, precision=10, scale=6), nullable=False)

    def __repr__(self):
        return f"{self.station_id} {self.measurementtype} {self.date} {self.value}"


class MeasurementTypeOfStation(Base):
    __tablename__ = "measurementtype_station"

    id = Column(SmallInteger, Sequence('measurementtype_sta_id_seq'), primary_key=True)
    # measurements = relationship('StationMeasurement', backref='measurement_type')
    # date_from = Column(Date)
    # date_to = Column(Date)
    name = Column(String(20), unique=True)
    description = Column(String(200))
    unit = Column(String(20))
    measurements = relationship('StationMeasurement', backref='type')

    def pretty_description(self):
        descr = self.description.replace("hoehe", "höhe")
        descr = lib.utils.remove_prefix(descr, "stdl ")
        descr = lib.utils.remove_prefix(descr, "stdl. ")
        return descr

    def __repr__(self):
        return f"{self.name} {self.description} [{self.unit}]"

    def __eq__(self, other):
        return other.name == self.name

    def __hash__(self):
        return self.name.__hash__()

# -----------------


class County(Base):
    __tablename__ = "county"

    id = Column(SmallInteger, Sequence('county_id_seq'), primary_key=True)
    name = Column(String(40), unique=True)
    measurements = relationship('CountyMeasurement', backref='county')

    def __repr__(self):
        return f"{self.name}"

    def __eq__(self, other):
        return other.name == self.name

    def __hash__(self):
        return self.name.__hash__()


class MeasurementTypeOfCounty(Base):
    __tablename__ = "measurementtype_county"

    id = Column(SmallInteger, Sequence('measurementtype_cnt_id_seq'), primary_key=True)
    # measurements = relationship('StationMeasurement', backref='measurement_type')
    # date_from = Column(Date)
    # date_to = Column(Date)
    name = Column(String(100), unique=True)
    unit = Column(String(20))
    measurements = relationship('CountyMeasurement', backref='type')

    def __repr__(self):
        return f"{self.name} [{self.unit}]"

    def __eq__(self, other):
        return other.name == self.name

    def __hash__(self):
        return self.name.__hash__()


class CountyMeasurement(Base):
    __tablename__ = "countymeasurement"
    # __table_args__ = (UniqueConstraint('county_id', 'measurementtype_id', 'year', 'month',
    #     name='countymeasurement_uc'),)

    id = Column(Integer, Sequence('countymeasurement_id_seq'), primary_key=True)
    county_id = Column(SmallInteger, ForeignKey("county.id"), nullable=False)
    measurementtype_id = Column(SmallInteger, ForeignKey("measurementtype_county.id"), nullable=False)
    year = Column(SmallInteger, nullable=False)
    month = Column(SmallInteger, nullable=False)
    value = Column(Numeric(asdecimal=False, precision=10, scale=6), nullable=False)

    def __repr__(self):
        return f"{self.id} {self.year} {self.month} {self.value}"


if __name__ == '__main__':
    print("Cannot call this module directly")
