import logging
import os
import re
import sys
import urllib.request
from abc import ABC, abstractmethod
from datetime import datetime, date
from io import IOBase
from pathlib import Path
from zipfile import ZipFile
import bz2
import gzip
import filelock
import requests
from sqlalchemy import func
import lib.dbmodels
import pathlib

monatsnamen = ("Januar", "Februar", "März", "April", "Mai", "Juni",
               "Juli", "August", "September", "Oktober", "November", "Dezember")

translation_en_de = {'year': 'Jahr',
                     'spring': "Frühling", 'summer': 'Sommer',
                     'autumn': 'Herbst', 'winter': 'Winter',
                     'hour': 'Stunde', 'month': 'Monat'}
for key, value in {f"month {m}" : monatsnamen[m - 1] for m in range(1, 12 + 1)}.items():
     translation_en_de[key] = value

# Siehe auch https://www.dwd.de/DE/service/lexikon/Functions/glossar.html
inventory_of_weather_phenomenon_types = {  # RR, SD, TM, ...
    "rr": ("Niederschlag", "mm"),
    "sd": ("Sonnenscheindauer", "h"),
    "tm": ("Lufttemperatur", "°C"),
    "tnas": ("Frosttage", "d"),  # Tmin < 0
    "tnes": ("Tropennächte", "d"),  # Tmin nachts >=20
    "txbs": ("Hitzetage", "d"),  # Tmax >= 30
    "txcs": ("Eistage", "d"),  # Tmax < 0
    "rrsfs": ("Niederschlag >=10mm", "d"),
    "rrsgs": ("Niederschlag >=20mm", "d"),
    "txas": ("Sommertage", "d"),  # Tmax >= 25
}


log = logging.getLogger(__name__)


def dict_sequence(dict):
    """dict(k,v) -> [k, v], where v can also be a list, then k is repeated for each entry.
        So, 'one'->[1,2,3] becomes (one,1),(one,2),(one,3)"""
    klist = []
    vlist = []
    for item in dict.items():
        for l in item[1]:
            klist.append(item[0])
            vlist.append(l)
    return klist, vlist


def linear(x, m, t):
    """Template function for the curve fitting. For this short time, a line is OK"""
    return m * x + t


def rolling_mean(list, num_averages):
    """In German: 'Gleitender Durchschnitt' of num_averages list entries """
    averaged_list = []
    if len(list) < num_averages:
        raise ValueError(
            f"List is too short. It contains {len(list)} elements, "
            f"but a rolling mean of width {num_averages} is wanted")
    for entry_number in range(num_averages, len(list) + 1):
        averaged_list.append(sum(list[entry_number - num_averages:entry_number]) / num_averages)
    return averaged_list


def split_into_chunks(measurements, value_name, chunkduration_seconds, sum_up_values=False):
    """Split a list into a number of elements that span a duration of chunkduration_seconds.
    Returns a list of a list of entries."""

    ret_val = []
    current_chunk = []
    oldest_datetime = datetime.fromtimestamp(0)  # Begin with a very old date
    for measurement in measurements:
        # logger.debug(f"measurement is {measurement}")
        seconds_diff = (measurement.date - oldest_datetime).total_seconds()
        if seconds_diff > chunkduration_seconds:
            if len(current_chunk) != 0:  # This applies to the first round
                if sum_up_values:
                    ret_val.append([sum(current_chunk)])
                else:
                    ret_val.append(current_chunk)
            oldest_datetime = measurement.date
            current_chunk = []
        current_chunk.append(measurement.value)
    if len(current_chunk) != 0:
        if sum_up_values:
            ret_val.append([sum(current_chunk)])
        else:
            ret_val.append(current_chunk)

    return ret_val


def reduce_list(deep_list):
    """Returns for example [1,2,3] when it receives [[1], [2], [3]],
    in other words it reduces the depth of the input list by 1"""
    ret_val = []
    for element_list in deep_list:
        for element in element_list:
            ret_val.append(element)
    return ret_val


def file_transparent_opener(file_name):
    """ Returns an open() method for this text file, even when it is compressed. """
    # Taken and modified from https://stackoverflow.com/questions/16813267/python-gzip-refuses-to-read-uncompressed-file
    # the answer of Rob Flickenger
    if file_name.endswith(".gz"):
        opener = gzip.open
    elif file_name.endswith(".bz2"):
        opener = bz2.open
    else:
        opener = open
    return opener


def get_files_in(remote_directory, recursively = False):
    """When there is a remote directory that contains HTML,
    get the directory of all the files as list"""
    log.debug(f"Asked for a list of files in {remote_directory} "
              f"{', recursively' if recursively else ''}")
    # remove a potential trailing slash
    if remote_directory.endswith("/"):
        remote_directory = remote_directory[:-1]
    with urllib.request.urlopen(remote_directory) as response:
        response_text = response.read()

    file_names = []
    lines = response_text.splitlines()
    for remote_entry_line in lines:
        file_name = remote_entry_line.decode("ascii").partition('<a href="')[2].partition('">')[0]
        if file_name is not None and file_name != "" and ".." not in file_name:
            if file_name.endswith("/"):  # A directory
                if recursively:
                    # Follow it, and append its files to the return list
                    file_names.extend(get_files_in(remote_directory + "/" + file_name, True))
                else:
                    # Take it
                    file_names.append(remote_directory + "/" + file_name)
            else:
                # It is a file name, so take it as is
                file_names.append(remote_directory + "/" + file_name)
    return file_names


def read_from_zip(zip_name, file_name):
    """Read the content of a file with the file_name out of the Zip file called zip_name"""
    with ZipFile(zip_name, "r") as zipfile:
        return zipfile.read(file_name)


def zip_directory(zip_name):
    """Read the directory of a Zip file"""
    return ZipFile(zip_name).namelist()


# The meteorologic seasons, beginning March, June, September, December each year
seasons = {"spring": (3, 4, 5), "summer": (6, 7, 8),
           "autumn": (9, 10, 11), "winter": (12, 1, 2),
           "year": (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, -1)}


def has_sufficient_entries(number_of_entries, months):
    """Given a number of entries, is it sufficient what was fetched from the DB?"""
    if number_of_entries == len(months):
        return True
    if -1 in months and (number_of_entries == len(months) - 1 or number_of_entries == 1):
        return True
    return False


def season_of_month(month):
    """Given a month, returns the season that this month is in (e.g. 4 -> 'spring')"""
    assert month >= 1
    assert month <= 12
    for season_range in seasons.items():
        if month in season_range[1]:
            return season_range[0]


def completely_in_year(month, season):
    """Returns a boolean if the season, starting from month, is completely available"""
    assert month >= 1
    assert month <= 12
    assert season in seasons.keys()
    return month <= seasons[season][0]


def are_seasons_possible(measurement_type_name):
    """Are seasons possible for this type? Eg, for 'Hitzetage', they're not."""
    if measurement_type_name in ("Lufttemperatur", "Niederschlag", "Sonnenscheindauer"):
        return True
    else:
        return False


def search_file(filename):
    """Find a file in PYTHONPATH. Return its absolute name, or None when it is not found.
    There is probably a much easier way to do this."""
    for search_dir in sys.path:
        if os.path.exists(os.path.join(search_dir, filename)):
            log.debug(f"Take the file {os.path.join(search_dir, filename)}")
            return os.path.join(search_dir, filename)
    return None


def sanitize_string(string):
    """Makes a string more readable"""
    replaced = string.replace('ue', 'ü')
    replaced = replaced.replace('oe', 'ö')
    replaced = replaced.replace('ae', 'ä')
    replaced = replaced.replace('daür', 'dauer')  # Blacklist
    return replaced


def desanitize_string(string):
    """Makes a string less readable, but conforming the content of the DB"""
    replaced = string.replace('ü', 'ue')
    replaced = replaced.replace('ö', 'oe')
    replaced = replaced.replace('ä', 'ae')
    return replaced



def contains_any_substring(haystack, sample_needles):
    """Returns whether any of the strings in sample_needles was found in the haystack string"""
    for item in sample_needles:
        if item in haystack:
            return True
    return False


def find_filename_recursively(file_name, additional_taboo_part=None):
    """Return the name of the file that exists in any subdirectory, or None.
    additional_part is only used for testing purposes. It is a part of the name that
    may not occur"""
    files = Path(".").rglob(str(file_name))
    cleaned_filename_list = []
    for current_file_name in files:
        if "venv" in str(file_name) or (additional_taboo_part is not None and
                                        additional_taboo_part not in str(current_file_name) and
                                        additional_taboo_part not in os.getcwd()):
            pass
        else:
            cleaned_filename_list.append(current_file_name)
    if len(cleaned_filename_list) == 0:
        log.warning(f"Could not find {file_name} in {os.getcwd()}")
        return None
    elif len(cleaned_filename_list) != 1:
        log.debug(f"Found more than one {file_name}: {cleaned_filename_list}. Taking the first one")

    return cleaned_filename_list[0]


def map_range(value_range, value):
    """Map a value in the range of value_range to another range that is defined inside the procedure"""
    # Stolen from https://rosettacode.org/wiki/Map_range#Python
    target_range = (200, 800)
    (a1, a2), (b1, b2) = value_range, target_range
    return b1 + ((value - a1) * (b2 - b1) / (a2 - a1))


def nearest_odd_number(number):
    """If 'number' is already odd, return it.
    If it is even, return number+1"""
    assert isinstance(number, int)
    if number // 2 == number / 2:
        return number + 1  # even
    return number  # odd


def remove_prefix(text, prefix):
    """Returns a new string missing a potential prefix.
    Cannot use the built-in one because it also has to run with Python 3.8"""
    if text.startswith(prefix):
        return text[len(prefix):]
    return text


def parse_date(date_string, assume_begin_of_timerange = True):
    """Take a string in German time format (e.g. '23.4.2023', and convert it to a date object.
    Works also with incomplete dates, like '2023' or '4.2023'.
    In this case, the begin or the end of the year or month are taken,
    depending on the second parameter.
    The special values 'past', 'future', 'now'/'today' are understood """
    if date_string == "past":
        return date.min
    if date_string == "future":
        return date.max
    if date_string in ("now", "today"):
        return date.today()
    p_year = re.compile(r"(\d\d\d\d)")
    p_year_month = re.compile(r"(\d+)\.(\d\d\d\d)")
    p_year_month_day = re.compile(r"(\d+)\.(\d+)\.(\d\d\d\d)")
    s_year_month_date = re.search(p_year_month_day, date_string)
    if s_year_month_date:
        return date(int(s_year_month_date.group(3)),
                    int(s_year_month_date.group(2)),
                    int(s_year_month_date.group(1)))
    s_year_month = re.search(p_year_month, date_string)
    if s_year_month:
        return date(int(s_year_month.group(2)),
                    int(s_year_month.group(1)),
                    1 if assume_begin_of_timerange else 31)
    s_year = re.search(p_year, date_string)
    if s_year:
        return date(int(s_year.group(1)),
                    1 if assume_begin_of_timerange else 12,
                    1 if assume_begin_of_timerange else 31)
    raise ValueError(f"Invalid date: {date_string}")  # No date parsed til now => Wrong format

def parse_timerange(start_stop):
    """the same like parse_date(), but with a range as input, like 'd1-d2'"""
    assert start_stop.count('-') == 1
    start_string, end_string = start_stop.split('-')
    stert_date = parse_date(start_string)
    end_date = parse_date(end_string, False)
    assert stert_date < end_date
    return stert_date, end_date


def reduce_list_density(list_like, density):
    """Return every density-th element of a list, including the last element"""
    return list_like[::-1][::density][::-1]


class Counter:
    """Returns true every <count> rounds"""

    def __init__(self, count):
        self.count = count
        self.start = count

    def round(self):
        """Every round, the counter is decreased or it flips; False/True is returned, accordingly"""
        self.count -= 1
        ret_val = self.count <= 0
        if ret_val:
            self.count = self.start
        return ret_val

    def __repr__(self):
        return f"Current counter: {self.count}"


class Aggregator(ABC):
    """Base class to aggregate a bunch of measurements"""

    def __init__(self):
        self.items = []

    def collect(self, item):
        self.items.append(item)

    def get_length(self):
        return len(self.items)

    @staticmethod
    def aggregator_factory(type_name):
        if "niederschlag" in type_name.lower() or "dauer" in type_name.lower():
            return SumAggregator()
        else:
            return AvgAggregator()

    @staticmethod
    def specific_aggregator(aggregator_type):
        if aggregator_type == "max":
            return MaxAggregator()
        elif aggregator_type == "min":
            return MinAggregator()
        elif aggregator_type == "avg":
            return AvgAggregator()
        elif aggregator_type == "sum":
            return SumAggregator()
        else:
            raise RuntimeError(f"Unknown aggregator: '{aggregator_type}'")

    def __str__(self):
        return f"[{self.items}]"

    @abstractmethod
    def aggregate(self):
        pass

    @abstractmethod
    def human_name(self):
        pass


class SumAggregator(Aggregator, ABC):
    """Sum up the measurements inside of a collection"""

    def aggregate(self):
        return sum(self.items)

    def human_name(self):
        return "Summe"


class AvgAggregator(Aggregator, ABC):
    """Calculate the average of the measurements inside of a collection"""

    def aggregate(self):
        return sum(self.items) / self.get_length()

    def human_name(self):
        return "Durchschnitt"


class MaxAggregator(Aggregator, ABC):
    """Calculate the average of the measurements inside of a collection"""

    def aggregate(self):
        return max(self.items)

    def human_name(self):
        return "Maximum"


class MinAggregator(Aggregator, ABC):
    """Calculate the average of the measurements inside of a collection"""

    def aggregate(self):
        return min(self.items)

    def human_name(self):
        return "Minimum"


class CachedURL:
    """The data is fetched from the given file, or the given URL.
    In this case, the file is created and filled with the URL content"""

    def __init__(self, cache_file, url):
        self.cache_file = cache_file
        self.url = url

    def get_content(self):
        if os.path.exists(self.cache_file):
            log.debug(f"The cache file {self.cache_file} exists. Use it")

            # Return the file content of the cache file
            with open(self.cache_file, 'rb') as cache:
                return cache.read()
        else:
            log.debug(f"The cache file '{self.cache_file}' does not exist. Create it")
            # Fetch content of the URL
            content = requests.get(self.url)
            log.debug(f'Got the content from the web of length {len(content.content)}')
            # Write the content into the cache file
            with open(self.cache_file, 'bw') as cache:
                cache.write(content.content)
            log.debug('Wrote the content into the cache file')
            # Return the content
            return content.content


class CachedStationDict:
    """Store the count of a station, which is defined by the ID.
    session: When querying an unknown station, fetch it from the database"""
    # todo Unit Tests
    def __init__(self, session):
        self.session = session
        self.so_far = {}

    def get(self, station_id):  # Returns the corresponding county ID
        """Get the county ID by the station ID"""
        log.debug(f"get({station_id})")
        assert station_id is not None
        cached_value = self.so_far.get(station_id, None)
        if cached_value is None:
            station = self.session.query(lib.dbmodels.Station).get(station_id)
            if station is None:
                max_station_id = self.session.query(func.max(lib.dbmodels.Station.id)).scalar()
                if max_station_id <= int(station_id):
                    raise ValueError(f"Could not find the station with the ID {station_id}")
                log.info(f"Search for station id {station_id}, "
                         f"but they only go up to {max_station_id}. Ignoring it")
                return None
            log.debug(f"{station=}")
            county = (self.session.query(lib.dbmodels.County).
                      filter(lib.dbmodels.County.name == desanitize_string(station.county)).first())
            assert county is not None, f"No county found for Station {station_id}"
            self.so_far[station_id] = county.id
            return county.id
        else:
            return cached_value

class PersistentCounter:
    """Counter that is stored in a file persistently"""
    def __init__(self, counter_filename: pathlib.Path):
        self.counter = ""
        self.counter_filename = counter_filename
        self.counter_lockfilename = counter_filename.with_suffix(".lock")
        self.lock = filelock.FileLock(self.counter_lockfilename, timeout=1)  # timeout in seconds
        self.load()
        self.write_counter()

    def load(self) -> None:
        """The current count is loaded from the file"""
        with self.lock:
            if self.counter_filename.exists():
                self.counter = self.counter_filename.read_text("utf8")
            else:
                self.counter = "0"

    def write_counter(self) -> None:
        """The current count is written to the file"""
        with self.lock:
            with self.counter_filename.open("wt", encoding="utf8") as counter_handle:
                counter_handle.write(self.counter)
                counter_handle.write("\n")

    def increment(self):
        """The count is incremented and written to the file"""
        with self.lock:
            self.load()
            counter_as_int = int(self.counter)
            counter_as_int += 1
            self.counter = str(counter_as_int)
            self.write_counter()


if __name__ == '__main__':
    print("Cannot call this module directly")
