#!/usr/bin/env python

import argparse
import math

from lib.connection import SessionServer
from lib.dbmodels import Station, MeasurementTypeOfStation
from lib.dbutils import measurements_exist, get_station_named, get_station_names_similar_to, get_all_station_mt
from lib.plotstation import PlotStationYear, PlotStationMonth, PlotStationHour

coordinates = (48.4253, 10.9417)  # Augsburg
desired_station_name = "Augsburg"


def distance_from_station(station, station_coordinates):
    return math.sqrt((station.geo_lat - station_coordinates[0]) ** 2 + (station.geo_lon - station_coordinates[1]) ** 2)


def distance(station):
    # logging.debug(f"desired station; {desired_station}, station: {station}")
    return distance_from_station(station, coordinates)


def degrees_to_km(degrees):
    return int(degrees * 120.0)  # Very approximate



def get_all_types():
    measurement_types = {}
    with SessionServer() as session:
        types_in_db = session.query(MeasurementTypeOfStation).all()
        for measurement_type in types_in_db:
            measurement_types[measurement_type.name] = measurement_type
    return measurement_types


def main():
    global desired_station_name
    global coordinates

    graph_station = get_station_named(desired_station_name)

    parser = argparse.ArgumentParser(description="Graph of various data "
                                                 "of DWD in a certain measurement station, by default Augsburg",
                                     epilog='Remember to run "persist.py" first!')
    parser.add_argument("--group", dest="group",
                        choices=["year", "month", "hour"],
                        default="year",
                        help="Group by what time range")
    parser.add_argument("--diagramtype", dest="diagramtype",
                        choices=["violin", "box"],
                        default="violin", help="The type of the plotted diagram")
    parser.add_argument("--base", dest="base",
                        choices=["year", "spring", "summer", "autumn", "winter"],
                        default="year", help="The measurements that are taken "
                                             "into account")
    parser.add_argument("--measurement", dest="measurement",
                        choices=[mt.name for mt in get_all_station_mt()],
                        default="TT_TU",
                        help="Which field of the measurement.")
    parser.add_argument("--station", dest="station",
                        default="Augsburg", help="The station of the measurement station")
    parser.add_argument("--stations", action='store_true',
                        help="List all available stations. " + \
                             "The ones with data available are marked with '*'")
    parser.add_argument("--closest_station", dest="closest_station",
                        help="Print the closest station to this coordinate. The separator is ':'")
    parser.add_argument("--image_name", dest="image_name",
                        type=str,
                        help="Output file name of the finished picture of the graph, "
                             "including its type indicator. "
                             "When it is not given, the graph is displayed on screen")

    args = parser.parse_args()
    temperature_type = get_all_types()["TT_TU"]  # Lufttemperatur
    if args.stations:
        with SessionServer() as session:
            stations = session.query(Station).order_by(Station.name).all()
            for graph_station in stations:
                print(f"{graph_station.name} (id={graph_station.id}; {graph_station.county})"
                      f" {'*' if measurements_exist(session, temperature_type, graph_station) else ''}")
            session.commit()
        exit(1)
    if args.station:
        with SessionServer() as session:
            graph_station = session.query(Station).filter(Station.name == args.station).first()
            session.commit()
            if graph_station is None:
                print(f"Unknown station '{args.station}'")
                exit(1)
            desired_station_name = graph_station.name
            coordinates = (graph_station.geo_lat, graph_station.geo_lon)
    if args.group == "hour":
        pclass = PlotStationHour
    elif args.group == "month":
        if args.base != "year":
            raise RuntimeError("Cannot plot the months using only a specific season")
        pclass = PlotStationMonth
    else:  # year
        pclass = PlotStationYear
    if args.closest_station:
        if ':' in args.closest_station:
            coordinates = (float(args.closest_station.split(':')[0]), float(args.closest_station.split(':')[1]))
        else:
            graph_station = get_station_named(args.closest_station)
            if graph_station is not None:
                coordinates = graph_station.geo_lat, graph_station.geo_lon
            else:
                print(f"Station not found: {args.closest_station}")
                similar_stations = get_station_names_similar_to(args.closest_station)
                if similar_stations is not None:
                    print(f"Did you mean one of these: {similar_stations}")
                exit(1)
        with SessionServer() as session:
            stations = session.query(Station).all()
            near_stations = sorted(stations, key=distance)
            print(', '.join(map(lambda x: f"{x} (around {degrees_to_km(distance(x))}km distance)", near_stations[:4])))
            for loc in near_stations:
                if measurements_exist(session, temperature_type, loc):
                    print(f"Nearest with data: {loc} (around {degrees_to_km(distance(loc))}km distance)")
                    break
            else:
                print("No station with entries found. Strange.")
        exit(1)
    p = pclass(base=args.base,
               diagram_type=args.diagramtype,
               measurement_type=get_all_types()[args.measurement],
               station=graph_station,
               image_name=args.image_name)
    p.do_it()


if __name__ == '__main__':
    main()
