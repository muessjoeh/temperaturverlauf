#!/usr/bin/env python
import os
import bz2
import logging
import urllib.error
from pathlib import Path
from urllib.request import urlopen
from tqdm import tqdm

import lib.conf
from lib.utils import get_files_in, contains_any_substring, inventory_of_weather_phenomenon_types
import lib.logger

configuration = lib.conf.Configuration()
log = logging.getLogger(__name__)
configuration.use_default_configuration_file()

# In which local directory to store the averages of each county
local_dir_county_averages = "data/county_averages"
# Where to find the measurement data remotely
remote_county_base_dir = "http://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/monthly/"
# Where to find the measurement data of weather phenomena remotely
remote_county_weather_phenomena_dir = "http://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/"
# Where to find the phenomena of the counties remotely
remote_station_phenomena_base_dir = "http://opendata.dwd.de/climate_environment/CDC/observations_germany/phenology/annual_reporters/"
# The constant remote station description file URLs and where to store them locally
remote_files = \
    {
        "http://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/"
        "precipitation/historical/RR_Stundenwerte_Beschreibung_Stationen.txt": "data/stations",
        "http://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/"
        "air_temperature/historical/TU_Stundenwerte_Beschreibung_Stationen.txt": "data/stations",
        "http://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/"
        "sun/historical/SD_Stundenwerte_Beschreibung_Stationen.txt": "data/stations",
        "https://opendata.dwd.de/climate_environment/CDC/observations_germany/phenology/"
        "annual_reporters/wild/historical/PH_Beschreibung_Phaenologie_Stationen_Jahresmelder.txt": "data/stations"
    }

# Where to store the measurements of the stations
local_dir_measurements = "data/measurements_of_station"
# The IDs of the stations that are fetched automatically
desired_stations = configuration.get("stations") or ()
measurement_station_urls = (
    "http://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/air_temperature/historical/",
    "http://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/air_temperature/recent/",

    "http://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/precipitation/historical/",
    "http://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/precipitation/recent/",

    "http://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/wind/historical/",
    "http://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/wind/recent/",

    "http://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/sun/historical/",
    "http://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/sun/recent/"
)

# phenomena of the stations out of which there will be built phenomena for the counties
desired_phenomena = configuration.get("phenomena") or ()


def find_substring(needle, haystack):
    """Return the part of the haystack that contains the needle"""
    for entry in haystack:
        if needle in entry:
            return entry
    return None


def collect_county_urls():
    """ Store the URLs of all the remote county files """
    measurement_files = get_files_in(remote_county_base_dir, True)
    for file in measurement_files:
        remote_files[file] = local_dir_county_averages


def collect_weather_phenomena_urls():
    """ Store the URLs of the weather phenomena, recursively """
    measurement_files = get_files_in(remote_county_weather_phenomena_dir, True)
    log.debug(f"measurement_files: {measurement_files}")
    for remote_file in measurement_files:
        if remote_file.endswith('.txt') and  '/regional_' in remote_file and \
                contains_any_substring(remote_file, inventory_of_weather_phenomenon_types.keys()):
            # When the name of the file matches some criteria, and it is of a translated (ie known) type, take it
            log.debug(f"Taking {remote_file} as {local_dir_county_averages}")
            remote_files[remote_file] = local_dir_county_averages


def collect_station_urls():
    """ Store the URLs of all the remote station files """
    not_found_substrings = []
    for measurement_dir_url in measurement_station_urls:
        # Fetch the file names in the remote directory
        remote_station_files = get_files_in(measurement_dir_url)
        for station_subname in desired_stations:
            found_name = find_substring(station_subname, remote_station_files)
            if found_name:
                found_url = f"{measurement_dir_url}/{os.path.basename(found_name)}"
                log.debug(f"found_url: {found_url}")
                remote_files[found_url] = local_dir_measurements
                log.debug(f"Found {station_subname} in {found_url}")
            else:
                not_found_substrings.append(station_subname)
    if not_found_substrings:
        log.warning(f"Did not find all measurements for the stations {set(not_found_substrings)}")

def collect_phenomena():
    """Store all URLs of configured phenomena files"""
    log.debug("Beginning of collect_phenomena()")
    # First, fetch file names of all known phenomena
    all_phenomenon_files = get_files_in(remote_station_phenomena_base_dir, True)
    log.debug(f"All phenomenon files: {all_phenomenon_files}")

    # Look for all files that match the desired species name
    potential_files = []
    for phenomenon in desired_phenomena:
        name, _, _ = phenomenon.split(";")
        potential_files.extend([file for file in all_phenomenon_files if "_" + name + "_" in file])
    if len(potential_files) == 2 * len(desired_phenomena):
        for file in potential_files:
            remote_files[file] = "data/station_phenomena"
    else:
        raise ValueError("Not all phenomena found. Is there a typo?")


def fetch_file(local_dir, remote_file):
    """ Actually fetch the remote_file, compress it and store it in local_dir.
    When it is ZIP file, take it as it is """
    log.info(f"Fetching {remote_file}")
    file_name = os.path.basename(remote_file)
    handle = urlopen(remote_file)
    content = handle.read()
    if file_name.endswith(".zip"):
        log.debug(f"The ZIP file name is {file_name}")

        with open(f"{local_dir}/{file_name}", "wb") as file:
            file.write(content)
    else:
        with open(f"{local_dir}/{file_name}.bz2", "wb") as file:
            compressed_content = bz2.compress(content, 9)
            file.write(compressed_content)
    log.debug("Wrote compressed file")


def create_local_directories():
    """ Create all the needed local directories """
    log.debug(f"Directories to create: {set(remote_files.values())}")
    for dir_to_create in set(remote_files.values()):
        log.debug(f"Creating directory {dir_to_create}")
        Path(dir_to_create).mkdir(parents=True, exist_ok=True)
        # parents: create also parent dirs; exist_ok: Don't complain when creating an existing dir
    Path(local_dir_measurements).mkdir(parents=True, exist_ok=True)


def main():
    print("Collecting files to download. Please be patient...")

    # Determine and remember the county measurement URLs
    collect_county_urls()

    # Determine and remember the weather phenomena measurement URLs
    collect_weather_phenomena_urls()

    # Determine and remember the actual ZIP file URLs
    collect_station_urls()

    # Get all the desired station phenomena
    collect_phenomena()

    # Create the necessary local directories
    create_local_directories()

    # Fetch files as-is
    log.debug(f"Processing {len(remote_files)} text files")
    for remote_url, local_dir in tqdm(remote_files.items(), desc='Remote files'):
        fetch_file(local_dir, remote_url)


if __name__ == '__main__':
    main()
